#define SPRITE_MEM_A	$E9B0		
#define SPRITE_MEM_B	$EDB0
#define KEYBOARD		$DF00
#define BUFFER_STAT		$DF04
#define VGA_DONE		$DF05
#define DIG_TABLE		$0033
#define CLOUD_COUNT		$08

; INIT DISP
; Description:	Will start the GPU on buffer A and the CPU on buffer B
InitDisp:
	; pre load the background to the display
	ld		hl,$E938
    ld		(SPRITE_MEM),hl
    call	Draw_Bg
    ld		hl,$ED38
    ld		(SPRITE_MEM),hl
    call	Draw_Bg
    ; initialize the display buffer addresses
	ld		a, $01
    ld		(CURRENT_BUFF),a
    ld		a, $01
    ld		(BUFFER_STAT),a	; set GPU to buffer A
    ld		hl,SPRITE_MEM_A
    ld		(SPRITE_MEM),hl	; set CPU to buffer B
    ret

; FLIP BUFFER
; Description:	Call this when you have written all new sprites to the memory
;	and are ready to display them to the screen
FlipBuffer:
	ld		a,(VGA_DONE)
    and		$01				;check if GPU is done
    jp		z,FlipBuffer	;if GPU is not done then wait
    ld		a,(KEYBOARD)
    and		1
    jp		z,noButton
    ld		a,1
    ld		(KEYBOARD_PRESS),a
noButton:
    ld		a,(CURRENT_BUFF)
    and		$01				; check if current buff is zero
    jp		z,switch_to_buff_b;if GPU is on buffer A
switch_to_buff_a:
    ld		a,$00
    ld		(CURRENT_BUFF),a
    ld		hl,SPRITE_MEM_B	
    ld		(SPRITE_MEM),hl	; set CPU to buffer  B
    ld		a, $00
    ld		(BUFFER_STAT),a	; set GPU to buffer  A
    jp		doneFlip
switch_to_buff_b:
    ld		a,$01
    ld		(CURRENT_BUFF),a
    ld		hl,SPRITE_MEM_A	
    ld		(SPRITE_MEM),hl	; set CPU to buffer  A
    ld		a, $01
    ld		(BUFFER_STAT),a	; set GPU to buffer  B
doneFlip:
; check the frame counter for overflow at 60
    ld		bc,(FRAME_COUNT)
    inc		bc			;increment the frame counter
    ld		a,c
    cp		28
    jp		nz,no_wrap_frame_cnt
    ld		a,$00
no_wrap_frame_cnt:
    ld		(FRAME_COUNT),a
	ret
    
; CLEAR FOREGROUND
; Description: this will clear all non background objects from both sprite buffer
Clr_Foreground:
; Clear first sprite buffer
	call 	FlipBuffer
    ld		hl,(SPRITE_MEM)
    ld		b,2
clrA:
	push	bc
    ld		b,254			; init counter
clr0:
	; Inner loop deletes ~31 sprites
		ld		(hl),$00		; clear memory
   		inc		hl
    	djnz	clr0
    pop		bc
    djnz	clrA
; Clear second sprite buffer
	call 	FlipBuffer
    ld		hl,(SPRITE_MEM)
    ld		b,2
clrB:
	push	bc
    ld		b,254			; init counter
clr1:
	; Inner loop deletes ~31 sprites
		ld		(hl),$00		; clear memory
    	inc		hl
    	djnz	clr1
    pop		bc
    djnz	clrB
    ret
    
; PUT NUMBER
; Inputs:		h contains the digit to display
;				l contains the line number
;				c contains the column number
PutNum:
    ld		e,9
    push	hl
    push	bc
    call	Mul8b
    ld		bc,DIG_TABLE
    add		hl,bc
    ld		(CHAR_OFFSET),hl	; stores the digit offset to print in RAM
    pop		bc
    pop		hl
    push	hl
    push	bc
    ld		h,l
    ld		e,10
    call	Mul8b	
    ld		(CHAR_Y),hl			; stores the digits x position
    pop		bc
    pop		hl
    ld		h,c
    ld		e,10
    call	Mul8b
    ld		(CHAR_X),hl			; stores the digits y position
    call	DrawDigit
    ret
    
    
; DRAW PIPE MIDDLE
; Inputs:	have the x and y location ram variables
DrawDigit:
	ld		hl,(SPRITE_MEM)
    ld		bc,$0808
    ld		(hl),c		; store width
    inc		hl
    ld		(hl),b		; store height
    inc		hl
    ld		bc,(CHAR_X)	; load x_pos from ram
    ld		(hl),c		; store x_l
    inc		hl
    ld		(hl),b		; store x_h
    inc		hl
    ld		bc,(CHAR_Y)	; load y_pos from ram
    ld		(hl),c		; store y_l
    inc		hl
    ld		(hl),b		; store y_h
    inc		hl
    ld		bc,(CHAR_OFFSET)
    ld		(hl),c		; store offset_l
    inc		hl
    ld		(hl),b		; store offset_h
    inc		hl
    ld		(SPRITE_MEM),hl
    ret
    
cloud_sprite_table:
	.db		$0C,$0F	;CLOUD_A
    .db		$28,$0F	;CLOUD_B
    .db		$C0,$58	;CLOUD_C
    .db		$0C,$0F	;CLOUD_A
cloud_width_table:
	.db		$1B		;CLOUD_A
    .db		$22		;CLOUD_B
    .db		$2C		;CLOUD_C
    .db		$1B		;CLOUD_A
cloud_height_table:
	.db		$0C		;CLOUD_A
    .db		$0A		;CLOUD_B
    .db		$13		;CLOUD_C
    .db		$0C		;CLOUD_A
    
Init_Clouds:
	ld		ix,CLOUD_0_SPRITE
    ld		b,CLOUD_COUNT	; init counter
init_next_cloud:
	push	bc
   	call	Get_Rand_Byte
    and		$06				; random values 0,2,4,6
    ld		c,a
    ld		b,0				; bc contains random offset
    ld		iy,cloud_sprite_table
    add		iy,bc			; iy contains address to random cloud sprite offset
    ld		e,(iy+0)
    ld		d,(iy+1)		; de contains random cloud sprite sheet offset
    ld		(ix+0),e
    ld		(ix+1),d		; store the random sprite
    srl		c				; divide by 2 to bring random scale to (0-3)
    ld		iy,cloud_width_table
    add		iy,bc			; iy contains address to random cloud width
    ld		e,(iy+0)		; e contains random cloud width
    ld		(ix+2),e		; store random sprite width
    ld		iy,cloud_height_table
    add		iy,bc			; iy contains address to random cloud height
    ld		e,(iy+0)		; e contains random cloud height
    ld		(ix+3),e		; store random sprite height
    call	Get_Rand_Byte
    srl		a
    srl		a
    add		a,20
    ld		(ix+6),a		; store random y_pos
    ld		hl,2560
    ld		(ix+4),l
    ld		(ix+5),h		; set init x to 320
    ld		bc,8
    add		ix,bc			; move onto next cloud
    pop		bc
    ld		a,CLOUD_COUNT
    add		a,1
    sub		b
    neg		
    ld		(ix+7),a		; store velocity
    djnz	init_next_cloud
    ret
    
Draw_Clouds:
	ld		ix,CLOUD_0_SPRITE
    ld		hl,(SPRITE_MEM)
    ld		b,CLOUD_COUNT	; init counter
draw_next_cloud:
    ld		d,(ix+2)
    ld		(hl),d			; store width
    inc		hl
    ld		d,(ix+3)
    ld		(hl),d			; store height
    inc		hl
    ld		e,(ix+4)
    ld		d,(ix+5)
    call	SRA_DE
    call	SRA_DE
    call	SRA_DE
    ld		(hl),e			; store x_l
    inc		hl
    ld		(hl),d			; store x_h
    inc		hl
    ld		d,(ix+6)
    ld		(hl),d			; store y_l
    inc		hl
    ld		d,$00
    ld		(hl),d			; store y_h
    inc		hl
    ld		d,(ix+0)
    ld		(hl),d			; store offset_l
    inc		hl
    ld		d,(ix+1)
    ld		(hl),d			; store offset_h
    inc		hl
    ld		de,8
    add		ix,de			; move onto next cloud
    djnz	draw_next_cloud
    ld		(SPRITE_MEM),hl
    ret

Move_Clouds:
	ld		ix,CLOUD_0_SPRITE
    ld		b,CLOUD_COUNT	; init counter
move_next_cloud:
	ld		e,(ix+7)
    ld		d,$FF			; load cloud v
    ld		l,(ix+4)
    ld		h,(ix+5)		; load cloud x
    add		hl,de
    ld		(ix+4),l
    ld		(ix+5),h		; store new x
    ld		a,h
    cp		$FF
    jp		nz,dont_wrap_cloud
    ld		a,l
    cp		-100
    jp		nz,dont_wrap_cloud
    ld		hl,2560
    ld		(ix+4),l
    ld		(ix+5),h		; wrap to right side of screen
dont_wrap_cloud:
	ld		de,8
    add		ix,de			; move onto next cloud
    djnz	move_next_cloud
    ret
    
PrintScore:
	ld		hl,(SCORE_TENS)
    ld		h,l
    ld		l,5
    ld		c,15
    call	PutNum
    ld		hl,(SCORE_ONES)
    ld		h,l
    ld		l,5
    ld		c,16
    call	PutNum
    ret
    
PrintHighScore:
	ld		hl,(SPRITE_MEM)
    ; HIGH
    ld		bc,$0820
    ld		(hl),c		; store width
    inc		hl
    ld		(hl),b		; store height
    inc		hl
    ld		bc,120
    ld		(hl),c		; store x_l
    inc		hl
    ld		(hl),b		; store x_h
    inc		hl
    ld		bc,140
    ld		(hl),c		; store y_l
    inc		hl
    ld		(hl),b		; store y_h
    inc		hl
    ld		bc,HIGH
    ld		(hl),c		; store offset_l
    inc		hl
    ld		(hl),b		; store offset_h
    inc		hl
    ; SCORE
    ld		bc,$0828
    ld		(hl),c		; store width
    inc		hl
    ld		(hl),b		; store height
    inc		hl
    ld		bc,155
    ld		(hl),c		; store x_l
    inc		hl
    ld		(hl),b		; store x_h
    inc		hl
    ld		bc,140
    ld		(hl),c		; store y_l
    inc		hl
    ld		(hl),b		; store y_h
    inc		hl
    ld		bc,SCORE
    ld		(hl),c		; store offset_l
    inc		hl
    ld		(hl),b		; store offset_h
    inc		hl
    ld		(SPRITE_MEM),hl
    ld		hl,(HI_SCORE_TENS)
    ld		h,l
    ld		l,14
    ld		c,20
    call	PutNum
    ld		hl,(HI_SCORE_ONES)
    ld		h,l
    ld		l,14
    ld		c,21
    call	PutNum
    ret
    
Draw_Gameover:
	ld		hl,(SPRITE_MEM)
    ld		bc,$0840
    ld		(hl),c		; store width
    inc		hl
    ld		(hl),b		; store height
    inc		hl
    ld		bc,128
    ld		(hl),c		; store x_l
    inc		hl
    ld		(hl),b		; store x_h
    inc		hl
    ld		bc,116
    ld		(hl),c		; store y_l
    inc		hl
    ld		(hl),b		; store y_h
    inc		hl
    ld		bc,GAME_OVER
    ld		(hl),c		; store offset_l
    inc		hl
    ld		(hl),b		; store offset_h
    inc		hl
    ld		(SPRITE_MEM),hl
    ret
    
Draw_DeadBird:
	ld		hl,(SPRITE_MEM)
    ld		bc,$100B
    ld		(hl),c		; store width
    inc		hl
    ld		(hl),b		; store height
    inc		hl
    ld		bc,(BIRD_X)
    ld		(hl),c		; store x_l
    inc		hl
    ld		(hl),b		; store x_h
    inc		hl
    ld		bc,(BIRD_Y)
    ld		(hl),c		; store y_l
    inc		hl
    ld		(hl),b		; store y_h
    inc		hl
    ld		bc,BIRD_DEAD
    ld		(hl),c		; store offset_l
    inc		hl
    ld		(hl),b		; store offset_h
    inc		hl
    ld		(SPRITE_MEM),hl
    ret
    
Draw_Bird:
	ld		hl,(SPRITE_MEM)
    ld		bc,(BIRD_W)
    ld		(hl),c		; store width
    inc		hl
    ld		(hl),b		; store height
    inc		hl
    ld		bc,(BIRD_X)
    ld		(hl),c		; store x_l
    inc		hl
    ld		(hl),b		; store x_h
    inc		hl
    ld		bc,(BIRD_Y)
    ld		(hl),c		; store y_l
    inc		hl
    ld		(hl),b		; store y_h
    inc		hl
    ld		bc,(BIRD_SPRITE)
    ld		(hl),c		; store offset_l
    inc		hl
    ld		(hl),b		; store offset_h
    inc		hl
    ld		(SPRITE_MEM),hl
    ret
    
Draw_Grass:
    ld		bc,(GRASS_OFFSET)
    dec		bc
    ld		a,c
    cp		$E8				;-24
    jp		nz,grassUpdate
    ld		bc,$0000
    jp		grassUpdateDone
grassUpdate:
    ld		bc,(GRASS_OFFSET)
    dec		bc					;make the grass lower by one
grassUpdateDone:
    ld		(GRASS_OFFSET),bc
    ld		(GRASS_X),bc
    ld		bc,220
    ld		(GRASS_Y),bc	; set grass intial block at lower left corner
	ld		b,15			; draw 15 grass blocks
drawgrass0:
    push	bc				; push counter
    call	Draw_Grass_Block
    ld		hl,(GRASS_X)
    ld		bc,24			
    add		hl,bc
    ld		(GRASS_X),hl
    pop		bc				; pop counter
    djnz	drawgrass0
    ret
    
Draw_Title:
	ld		hl,(SPRITE_MEM)
    ; TITLE
    ld		bc,$187F
    ld		(hl),c		; store width
    inc		hl
    ld		(hl),b		; store height
    inc		hl
    ld		bc,96
    ld		(hl),c		; store x_l
    inc		hl
    ld		(hl),b		; store x_h
    inc		hl
    ld		bc,77
    ld		(hl),c		; store y_l
    inc		hl
    ld		(hl),b		; store y_h
    inc		hl
    ld		bc,TITLE_OFFSET
    ld		(hl),c		; store offset_l
    inc		hl
    ld		(hl),b		; store offset_h
    inc		hl
    ; CREDIT
    ld		bc,$1C52
    ld		(hl),c		; store width
    inc		hl
    ld		(hl),b		; store height
    inc		hl
    ld		bc,119
    ld		(hl),c		; store x_l
    inc		hl
    ld		(hl),b		; store x_h
    inc		hl
    ld		bc,106
    ld		(hl),c		; store y_l
    inc		hl
    ld		(hl),b		; store y_h
    inc		hl
    ld		bc,CREDIT_OFFSET
    ld		(hl),c		; store offset_l
    inc		hl
    ld		(hl),b		; store offset_h
    inc		hl
    ld		(SPRITE_MEM),hl
    ret
    
Draw_Grass_Block:
	ld		hl,(SPRITE_MEM)
    ld		bc,$1317
    ld		(hl),c		; store width
    inc		hl
    ld		(hl),b		; store height
    inc		hl
    ld		bc,(GRASS_X)
    ld		(hl),c		; store x_l
    inc		hl
    ld		(hl),b		; store x_h
    inc		hl
    ld		bc,(GRASS_Y)
    ld		(hl),c		; store y_l
    inc		hl
    ld		(hl),b		; store y_h
    inc		hl
    ld		bc,$0C18
    ld		(hl),c		; store offset_l
    inc		hl
    ld		(hl),b		; store offset_h
    inc		hl
    ld		(SPRITE_MEM),hl
    ret