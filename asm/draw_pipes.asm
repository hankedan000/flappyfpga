; DRAW ALL PIPES
; Description:	this will draw all the pipes and move them forward one pixel
Move_And_Redraw_Pipes:
    ld		hl,(PIPE_0_X)
    dec		hl
    ld		(PIPE_0_X),hl	; move pipe forward one pixel
    ld		bc,108
    adc		hl,bc
    jp		nz,move_pipe_1
    ld		hl,320
    ld		(PIPE_0_X),hl	; warp to the right side if x=-107
    call	Get_Rand_Height
    ld		(PIPE_0_HEIGHT),a
move_pipe_1:
	ld		hl,(PIPE_1_X)
    dec		hl
    ld		(PIPE_1_X),hl	; move pipe forward one pixel
    ld		bc,108
    adc		hl,bc
    jp		nz,move_pipe_2
    ld		hl,320
    ld		(PIPE_1_X),hl	; warp to the right side if x=-107
    call	Get_Rand_Height
    ld		(PIPE_1_HEIGHT),a
move_pipe_2:
	ld		hl,(PIPE_2_X)
    dec		hl
    ld		(PIPE_2_X),hl	; move pipe forward one pixel
    ld		bc,108
    adc		hl,bc
    jp		nz,move_pipe_3
    ld		hl,320
    ld		(PIPE_2_X),hl	; warp to the right side if x=-107
    call	Get_Rand_Height
    ld		(PIPE_2_HEIGHT),a
move_pipe_3:
	ld		hl,(PIPE_3_X)
    dec		hl
    ld		(PIPE_3_X),hl	; move pipe forward one pixel
    ld		bc,108
    adc		hl,bc
    jp		nz,Draw_Pipes
    ld		hl,320
    ld		(PIPE_3_X),hl	; warp to the right side if x=-107
    call	Get_Rand_Height
    ld		(PIPE_3_HEIGHT),a
Draw_Pipes:
	ld		hl,PIPE_0_HEIGHT
    call	Draw_Pipe
    ld		hl,PIPE_1_HEIGHT
    call	Draw_Pipe
    ld		hl,PIPE_2_HEIGHT
    call	Draw_Pipe
    ld		hl,PIPE_3_HEIGHT
    call	Draw_Pipe
    ret

; GET RANDOM HEIGHT
; Outputs:	a contains a random pipe height 2-6
Get_Rand_Height:
	ld		a,($DF06)
    ld		bc,(BIRD_V)
    xor		c
    ld		bc,(BIRD_Y)
    xor		c
    srl		a
    and		$03
    add		a,2
    ret

; DRAW PIPE
; Inputs:	hl 	contains the address to pipe height in ram
Draw_Pipe:
	ld		(PIPE_ADDR),hl 	; store the address for future use
	ld		b,(hl)			; load the pipe height
    inc		hl
    ld		e,(hl)
    inc		hl
    ld		d,(hl)			; load the pipe's x_pos
    ld		(PIPE_TEMP_X),de
    ; INCREMENT SCORE
    push	bc
    push	de
    ld		h,d
    ld		l,e
    ld		bc,64
    sbc		hl,bc
    jp		nz,skip_score
    call	IncScore
skip_score:
    pop		de
    pop		bc
	ld		hl,200			; ground level
draw_pipe_l_con:
    ld		(PIPE_TEMP_Y),hl
    push	bc
    push	hl
    call	Draw_Pipe_Mid
    pop		hl
    ld		bc,-20			; -20 connector height
    add		hl,bc			; increment connector height
    pop		bc
    djnz	draw_pipe_l_con
    ld		bc,7
    add		hl,bc			; correct the pipe
    ld		(PIPE_TEMP_Y),hl
    push	hl
    call	Draw_Pipe_Bot 	; draw the top of the bottom piece
    pop		hl
    ld		bc,-70		; 60 pipe gap
    add		hl,bc
    ld		(PIPE_TEMP_Y),hl
    push	hl
    call	Draw_Pipe_Top 	; draw the bottom of the top piece
    pop		hl
draw_pipe_h_con:
	ld		bc,-20		; -20
    add		hl,bc
    ld		(PIPE_TEMP_Y),hl
    push	hl
    call	Draw_Pipe_Mid 	; draw the upper pieces until off the screen
    pop		hl
    bit		7,h
    jp		z,draw_pipe_h_con
    ret		
	
; DRAW PIPE TOP
; Inputs:	have the x and y location ram variables
Draw_Pipe_Top:
	ld		hl,(SPRITE_MEM)
    ld		bc,$0C19
    ld		(hl),c		; store width
    inc		hl
    ld		(hl),b		; store height
    inc		hl
    ld		bc,(PIPE_TEMP_X)			; load x_pos from ram
    ld		(hl),c		; store x_l
    inc		hl
    ld		(hl),b		; store x_h
    inc		hl
    ld		bc,(PIPE_TEMP_Y)			; load y_pos from ram
    ld		(hl),c		; store y_l
    inc		hl
    ld		(hl),b		; store y_h
    inc		hl
    ld		bc,$3556
    ld		(hl),c		; store offset_l
    inc		hl
    ld		(hl),b		; store offset_h
    inc		hl
    ld		(SPRITE_MEM),hl
    ret

; DRAW PIPE BOTTOM
; Inputs:	have the x and y location ram variables
Draw_Pipe_Bot:
	ld		hl,(SPRITE_MEM)
    ld		bc,$0C19
    ld		(hl),c		; store width
    inc		hl
    ld		(hl),b		; store height
    inc		hl
    ld		bc,(PIPE_TEMP_X)			; load x_pos from ram
    ld		(hl),c		; store x_l
    inc		hl
    ld		(hl),b		; store x_h
    inc		hl
    ld		bc,(PIPE_TEMP_Y)			; load y_pos from ram
    ld		(hl),c		; store y_l
    inc		hl
    ld		(hl),b		; store y_h
    inc		hl
    ld		bc,$2516
    ld		(hl),c		; store offset_l
    inc		hl
    ld		(hl),b		; store offset_h
    inc		hl
    ld		(SPRITE_MEM),hl
    ret
    
; DRAW PIPE MIDDLE
; Inputs:	have the x and y location ram variables
Draw_Pipe_Mid:
	ld		hl,(SPRITE_MEM)
    ld		bc,$1319
    ld		(hl),c		; store width
    inc		hl
    ld		(hl),b		; store height
    inc		hl
    ld		bc,(PIPE_TEMP_X)			; load x_pos from ram
    ld		(hl),c		; store x_l
    inc		hl
    ld		(hl),b		; store x_h
    inc		hl
    ld		bc,(PIPE_TEMP_Y)			; load y_pos from ram
    ld		(hl),c		; store y_l
    inc		hl
    ld		(hl),b		; store y_h
    inc		hl
    ld		bc,$0BFE
    ld		(hl),c		; store offset_l
    inc		hl
    ld		(hl),b		; store offset_h
    inc		hl
    ld		(SPRITE_MEM),hl
    ret