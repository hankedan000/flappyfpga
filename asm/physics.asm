#define TIMER 			$DF02
#define RAND			$DF06

; UPDATE PHYSICS
; Description:	updates the velocity and position of the bird
Update_Physics:
    ld		hl,(BIRD_V) 
    ld		de,$01FF
    sbc		hl,de
    jp		z,max_vel
    ld		hl,(BIRD_V)
	ld		bc,(BIRD_A) 
    add		hl,bc		; add the acceleration the bird_v
    ld		(BIRD_V),hl	; store new velocity
max_vel:
	ld		bc,(BIRD_V)
    ld		hl,(BIRD_Y_TEMP)	; shift velocity twice (divide by 2)
    add		hl,bc		; add the velocity to the bird_y
    ld		(BIRD_Y_TEMP),hl	; store new velocitycall
    call	SRA_HL
    call	SRA_HL
    call	SRA_HL
    call	SRA_HL
    call	SRA_HL
    ld		(BIRD_Y),hl
    ret

; GROUND COLLISION
; Description:	checks the position for collision with the ground
; Result:		If collision detected then program jumps to GAMEOVER
Ground_Collision:
	ld		hl,(BIRD_Y)	;get y position
    ld		bc,-208
    add		hl,bc		;subtract 220
    ld		a,$80
    and		h
    jp		nz,groundCollisonFalse	;did not collide
    ld		bc,203
    ld		(BIRD_Y),bc
    pop		bc						;throws away the return address
    jp		GameOver
groundCollisonFalse:
    ret
    
; PIPE COLLISION
; Description:	checks to see if the bird hits any of the pipes
; Result:		if a collision is detected then the program jumps to GameOver
Pipe_Collision:
	ld		ix,PIPE_0_HEIGHT
    ld		b,4				; counter
Next_Pipe:
	push	bc
    ; CHECK THAT PIPE_X IS WITH A BYTE
    ld		a,(ix+2)
    cp		0
    jp		nz,skip_pipe	; skip this pipe if the high byte of PIPE_X is not zeros
    ld		a,(ix+1)
    cp		120
    jp		nc,skip_pipe	; skip this pipe if the lower byte of PIPE_X is within range
    ld		h,(ix+0)		; h contains pipe_#_height
    ld		e,20
    call	Mul8b
    ld		b,h
    ld		c,l				; move product hl to bc
    ld		hl,194
    or		a				; clear the carry flag
    sbc		hl,bc
    push	hl				; push lower bounding line
    ld		bc,47
    or		a				; clear the carry flag
    sbc		hl,bc
    push	hl				; push upper bounding line
    ld		a,(ix+1)		; a contains low pipe_#_x
    sub		16				; subtract 17 from pipe_#_x
    push	af				; push left bounding line
    add		a,39
   	; CHECK RIGHT BOUND
    ld		hl,(BIRD_X)
    ld		d,$00
    cp		l
    jp		c,past_right_bound
    set		0,d				; set left of right bound
past_right_bound:
	pop		af				; pop left bounding line
    ; CHECK LEFT BOUND
    cp		l
    jp		nc,before_left_bound
    set		1,d				; set right of left bound
before_left_bound:
	pop		bc				; pop upper bounding line
    ld		a,c
    ; CHECK UPPER BOUND
    ld		hl,(BIRD_Y)
    cp		l
    jp		c,below_upper_bound
    set		2,d				; set above upper bound
below_upper_bound:
	pop		bc				; pop lower bounding line
    ld		a,c
    ; CHECK LOWER BOUND
    cp		l
    jp		nc,above_lower_bound
    set		3,d				; set below lower bound
above_lower_bound:
    ld		a,d
    cp		$0B
    jp		z,GameOverCollision
    ld		a,d
    cp		$07
    jp		z,GameOverCollision
skip_pipe:
    inc		ix
    inc		ix
    inc		ix				; ix contains next pipe height address
	pop		bc
    dec		b
	jp		nz,Next_Pipe
    ret
GameOverCollision:
	pop		bc				; pop extra push of bc
    pop		bc				; pop extra retern address
    jp		DeathFall

; GET TIME
; Inputs:	hl contains the address to objects old time (ie (BIRD_T))
; Outputs:	bc contains the delta time (ms) since the previous call
GetTime:
	push 	hl			;push objects time location
	ld		c,(hl)
    inc		hl
    ld		b,(hl)		;load bc with objects old time
	ld		hl,(TIMER)	;load the current time
    sbc		hl,bc		;hl contains the delta time
    jp		nc,time_done;finish if no overflow
    ld		bc,$FFFF
    add		hl,bc		;hl contains the corrected delta time
time_done:
	push	hl
    pop		bc			;move result to bc
    pop		de			;pop objects time location
    ld		hl,(TIMER)
    ld		a,l
    ld		(de),a		;store objects new lower timer value
    inc		de
    ld		a,h
    ld		(de),a		;store objects new upper timer value
    ret
    
; GET RANDOM BYTE
; Outputs:	a contains a random byte
Get_Rand_Byte:
	ld		a,(RAND)
    ld		bc,(BIRD_V)
    xor		c
    ld		bc,(BIRD_Y)
    xor		c
    ret
    
; PAUSE
; Inputs:	bc contains the delay time (ms)
Pause:
	ld		hl,(TIMER)		;get init time
    ld		(PAUSE_TEMP),hl	;store init time in ram
pause0:
    ld		hl,(TIMER)		;load current time
    ld		de,(PAUSE_TEMP)	;load init time
    sbc		hl,de			;hl contains delta time
    sbc		hl,bc			;will be negative if time has not elapsed
    bit		7,h				;check if negative number
    jp		nz,pause0
    ret
    
Mul8b:                           ; this routine performs the operation HL=H*E
  	ld d,0                         ; clearing D and L
  	ld l,d
  	ld b,8                         ; we have 8 bits
Mul8bLoop:
  	add hl,hl                      ; advancing a bit
  	jp nc,Mul8bSkip                ; if zero, we skip the addition (jp is used for speed)
  	add hl,de                      ; adding to the product if necessary
Mul8bSkip:
  	djnz Mul8bLoop
  	ret
    
SRA_HL:
	sra		h
    jp		nc,skip_sra_hl
	srl		l
    set		7,l
    ret
skip_sra_hl:
	srl		l
	ret
    
SRA_BC:
	sra		b
    jp		nc,skip_sra_bc
	srl		c
    set		7,c
    ret
skip_sra_bc:
	srl		c
	ret
    
SRA_DE:
	sra		d
    jp		nc,skip_sra_de
	srl		e
    set		7,e
    ret
skip_sra_de:
	srl		e
	ret