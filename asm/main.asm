#include "ram_def.asm"
#define BIRD_FLAP_0			$0000
#define BIRD_FLAP_1			$0011
#define BIRD_FLAP_2			$0022
#define BIRD_DEAD			$0F00
#define SHIP				$1F53
#define SHIP_DEAD			$3213
#define	ANDROID				$2440
#define JELLY				$4240
#define CLOUD_A				$0F0C
#define CLOUD_B				$0F28
#define	CLOUD_C				$58C0
#define GAME_OVER			$008D
#define HIGH				$00CE
#define SCORE				$0BD5
#define TITLE_OFFSET		$20A4
#define CREDIT_OFFSET		$71C0
#define SKY_OFFSET			$00F0
#define CITY_OFFSET			$4BB0
#define KEYBOARD			$DF00
#define	LINE_OFFSET			$6EC2

SETUP:
    ld		SP,$FFFF		; load stack pointer
	call	InitDisp
    call	Init_Clouds
    call	Clr_Foreground
    call	Reset_Pipes
    call	Reset_Bird
    
    ; START GRASS AT ZERO
    ld		hl,$0000
    ld		(GRASS_OFFSET),hl
    ; RESET SCORE
    ld		bc,$0000
    ld		(SCORE_ONES),bc
    ld		(HI_SCORE_ONES),bc
    
BOOT_SCREEN:
	call	Change_Bird_Sprite
    call	Draw_Grass
    call	Draw_Title
    call	FlipBuffer
    ; CHECK KEYBOARD
    ld		a,(KEYBOARD_PRESS)
    cp		1
    jp		nz,BOOT_SCREEN
    ; RESET BUTTON PRESS
    ld		a,0
    ld		(KEYBOARD_PRESS),a
    ld		hl,$0002		; a=1
    ld		(BIRD_A),hl
    ; RESET SCORE
    ld		bc,$0000
    ld		(SCORE_ONES),bc
    call	Clr_Foreground
    jp		MAIN
    
MAIN:
;	call	Move_Clouds
;	call	Draw_Clouds
	call	Update_Physics
    call	Ground_Collision
    call	Pipe_Collision
    call	Move_And_Redraw_Pipes
    call	Draw_Grass
    call	Change_Bird_Sprite
    call	PrintScore
    call	FlipBuffer
	; CHECK KEYBOARD
    ld		a,(KEYBOARD_PRESS)
    cp		1
    jp		nz,dontFlap
    ld		hl,$FFBE		; v=-2.0625
    ld		(BIRD_V),hl
    add		hl,bc
    ld		(BIRD_Y),hl
    ; RESET BUTTON PRESS
    ld		a,0
    ld		(KEYBOARD_PRESS),a
dontFlap:
    jp		MAIN
    

    
SetHighScore:
	ld		a,(SCORE_TENS)
    ld		h,a
    ld		a,(SCORE_ONES)
    ld		l,a
    ld		a,(HI_SCORE_TENS)
    ld		b,a
    ld		a,(HI_SCORE_ONES)
    ld		c,a
    or		1			; clear carry
    sbc		hl,bc
    bit		7,h
    jp		nz,not_high_score
    ld		a,(SCORE_TENS)
    ld		(HI_SCORE_TENS),a
    ld		a,(SCORE_ONES)
    ld		(HI_SCORE_ONES),a
not_high_score:
	ret

; INCREMENT SCORE
; Description:	adds one to the score
IncScore:
	ld		a,(SCORE_ONES)
    cp		9
    jp		nz,dont_inc_tens
	ld		hl,SCORE_TENS
    inc		(hl)
    ld		a,0
    ld		(SCORE_ONES),a
    ret
dont_inc_tens:
	ld		hl,SCORE_ONES
    inc		(hl)
    ret

DeathFall:
	call	Update_Physics
    call	Ground_Collision
	call	Draw_Pipes
    call	Draw_DeadBird
    call	Draw_Grass
    call	Draw_Gameover
    call	PrintScore
	call	FlipBuffer
    ld		bc,(GRASS_OFFSET)
    inc		bc
    ld		(GRASS_OFFSET),bc	; if not done the grass won't be the same on each frame
    jp		DeathFall

GameOver:
	call	SetHighScore
    call	Draw_Pipes
    call	Draw_DeadBird
    call	Draw_Grass
    call	Draw_Gameover
    call	PrintScore
    call	PrintHighScore
	call	FlipBuffer
    ld		bc,(GRASS_OFFSET)
    inc		bc
    ld		(GRASS_OFFSET),bc	; if not done the grass won't be the same on each frame
    call	Draw_Pipes
    call	Draw_DeadBird
    call	Draw_Grass
    call	Draw_Gameover
    call	PrintScore
    call	PrintHighScore
    call	FlipBuffer
    ; RESET BUTTON PRESS
    ld		a,0
    ld		(KEYBOARD_PRESS),a
    ; PAUSE
    ld		bc,250				; 250ms
    call	Pause
wait_for_new_game:
	call	FlipBuffer
    ; CHECK KEYBOARD
    ld		a,(KEYBOARD_PRESS)
    cp		1
    jp		nz,wait_for_new_game
    ; RESET BUTTON PRESS
    ld		a,0
    ld		(KEYBOARD_PRESS),a
    call	Clr_Foreground
    call	Reset_Bird
    call	Reset_Pipes
    jp		BOOT_SCREEN
    
Reset_Pipes:
    ld		hl,320
    ld		(PIPE_0_X),hl
    call	Get_Rand_Height
    ld		(PIPE_0_HEIGHT),a
    ld		hl,428
    ld		(PIPE_1_X),hl
    call	Get_Rand_Height
    ld		(PIPE_1_HEIGHT),a
    ld		hl,536
    ld		(PIPE_2_X),hl
    call	Get_Rand_Height
    ld		(PIPE_2_HEIGHT),a
    ld		hl,644
    ld		(PIPE_3_X),hl
    call	Get_Rand_Height
    ld		(PIPE_3_HEIGHT),a
    ret
    
Reset_Bird:
	ld		hl,0			
    ld		(BIRD_A),hl		; a=0
    ld		hl,$0000
    ld		(BIRD_V),hl		; v=00
    ld		hl,70
    ld		(BIRD_X),hl		; x=70
    ld		hl,50
    ld		(BIRD_Y),hl		; y=50
    ld		hl,$0640		
    ld		(BIRD_Y_TEMP),hl; y_temp=50
    ld		hl,BIRD_FLAP_0
    ld		(BIRD_SPRITE),hl; flap 0 sprite
    ld		a,$10
    ld		(BIRD_W),a
    ld		a,$0B
    ld		(BIRD_H),a
    ld		(EASTER_BIRD),a
    ret

Change_Bird_Sprite:
	ld		a,(EASTER_BIRD)
    cp		$FF
    jp		z,birdEnd
	ld		a,($E4CA)		; bird_y_h
    cp		$FF
    jp		nz,regular_bird
    ld		a,(BIRD_Y)
    cp		$CE
    jp		z,regular_bird
    ld		a,(HI_SCORE_ONES)
    cp		3
    jp		nz,regular_bird
    ld		a,$FF
    ld		(EASTER_BIRD),a	; set flag
    ld		hl,ANDROID
    ld		(BIRD_SPRITE),hl
    ld		a,$12
    ld		(BIRD_W),a
    ld		a,$17
    ld		(BIRD_H),a
    jp		birdEnd
regular_bird:
	ld		a,$10
    ld		(BIRD_W),a
    ld		a,$0B
    ld		(BIRD_H),a
	ld		a,(FRAME_COUNT)
    and		$FFFF				;check 0
    jp		nz,birdCheck2
    ld		hl,BIRD_FLAP_0		;set drawbirdflap0
    ld		(BIRD_SPRITE),hl
  	jp		birdEnd
birdCheck2:
	cp		7					; if(a=7)
    jp		nz,birdCheck3
    ld		hl,BIRD_FLAP_1		;set drawbirdflap1
    ld		(BIRD_SPRITE),hl
  	jp		birdEnd
birdCheck3:
	cp		14					; if(a=14)
    jp		nz,birdCheck4
    ld		hl,BIRD_FLAP_2		;set drawbirdflap2
    ld		(BIRD_SPRITE),hl
  	jp		birdEnd
birdCheck4:
	cp		21					; if(a=21)
    jp		nz,birdEnd
    ld		hl,BIRD_FLAP_1		;set drawbirdflap1
    ld		(BIRD_SPRITE),hl
birdEnd:
	call	Draw_Bird			;draw the bird
	ret
    
; Endless loop
QUIT:
    jp		QUIT
    
#include "draw_pipes.asm"
#include "draw_bg.asm"
#include "physics.asm"
#include "graphics.asm"