Draw_Bg:
	ld		hl,(SPRITE_MEM)
	; 0,0 sky
	ld		bc,$364F
    ld		(hl),c		; store width
    inc		hl
    ld		(hl),b		; store height
    inc		hl
    ld		bc,0
    ld		(hl),c		; store x_l
    inc		hl
    ld		(hl),b		; store x_h
    inc		hl
    ld		bc,0
    ld		(hl),c		; store y_l
    inc		hl
    ld		(hl),b		; store y_h
    inc		hl
    ld		bc,SKY_OFFSET
    ld		(hl),c		; store offset_l
    inc		hl
    ld		(hl),b		; store offset_h
    inc		hl
    ; 1,0 sky
	ld		bc,$364F
    ld		(hl),c		; store width
    inc		hl
    ld		(hl),b		; store height
    inc		hl
    ld		bc,80
    ld		(hl),c		; store x_l
    inc		hl
    ld		(hl),b		; store x_h
    inc		hl
    ld		bc,0
    ld		(hl),c		; store y_l
    inc		hl
    ld		(hl),b		; store y_h
    inc		hl
    ld		bc,SKY_OFFSET
    ld		(hl),c		; store offset_l
    inc		hl
    ld		(hl),b		; store offset_h
    inc		hl
    ; 2,0 sky
	ld		bc,$364F
    ld		(hl),c		; store width
    inc		hl
    ld		(hl),b		; store height
    inc		hl
    ld		bc,160
    ld		(hl),c		; store x_l
    inc		hl
    ld		(hl),b		; store x_h
    inc		hl
    ld		bc,0
    ld		(hl),c		; store y_l
    inc		hl
    ld		(hl),b		; store y_h
    inc		hl
    ld		bc,SKY_OFFSET
    ld		(hl),c		; store offset_l
    inc		hl
    ld		(hl),b		; store offset_h
    inc		hl
    ; 3,0 sky
	ld		bc,$364F
    ld		(hl),c		; store width
    inc		hl
    ld		(hl),b		; store height
    inc		hl
    ld		bc,240
    ld		(hl),c		; store x_l
    inc		hl
    ld		(hl),b		; store x_h
    inc		hl
    ld		bc,0
    ld		(hl),c		; store y_l
    inc		hl
    ld		(hl),b		; store y_h
    inc		hl
    ld		bc,SKY_OFFSET
    ld		(hl),c		; store offset_l
    inc		hl
    ld		(hl),b		; store offset_h
    inc		hl
    ; 0,1 sky
	ld		bc,$364F
    ld		(hl),c		; store width
    inc		hl
    ld		(hl),b		; store height
    inc		hl
    ld		bc,0
    ld		(hl),c		; store x_l
    inc		hl
    ld		(hl),b		; store x_h
    inc		hl
    ld		bc,55
    ld		(hl),c		; store y_l
    inc		hl
    ld		(hl),b		; store y_h
    inc		hl
    ld		bc,SKY_OFFSET
    ld		(hl),c		; store offset_l
    inc		hl
    ld		(hl),b		; store offset_h
    inc		hl
    ; 1,1 sky
	ld		bc,$364F
    ld		(hl),c		; store width
    inc		hl
    ld		(hl),b		; store height
    inc		hl
    ld		bc,80
    ld		(hl),c		; store x_l
    inc		hl
    ld		(hl),b		; store x_h
    inc		hl
    ld		bc,55
    ld		(hl),c		; store y_l
    inc		hl
    ld		(hl),b		; store y_h
    inc		hl
    ld		bc,SKY_OFFSET
    ld		(hl),c		; store offset_l
    inc		hl
    ld		(hl),b		; store offset_h
    inc		hl
    ; 2,1 sky
	ld		bc,$364F
    ld		(hl),c		; store width
    inc		hl
    ld		(hl),b		; store height
    inc		hl
    ld		bc,160
    ld		(hl),c		; store x_l
    inc		hl
    ld		(hl),b		; store x_h
    inc		hl
    ld		bc,55
    ld		(hl),c		; store y_l
    inc		hl
    ld		(hl),b		; store y_h
    inc		hl
    ld		bc,SKY_OFFSET
    ld		(hl),c		; store offset_l
    inc		hl
    ld		(hl),b		; store offset_h
    inc		hl
    ; 3,1 sky
	ld		bc,$364F
    ld		(hl),c		; store width
    inc		hl
    ld		(hl),b		; store height
    inc		hl
    ld		bc,240
    ld		(hl),c		; store x_l
    inc		hl
    ld		(hl),b		; store x_h
    inc		hl
    ld		bc,55
    ld		(hl),c		; store y_l
    inc		hl
    ld		(hl),b		; store y_h
    inc		hl
    ld		bc,SKY_OFFSET
    ld		(hl),c		; store offset_l
    inc		hl
    ld		(hl),b		; store offset_h
    inc		hl
    ; 0,2 sky
	ld		bc,$364F
    ld		(hl),c		; store width
    inc		hl
    ld		(hl),b		; store height
    inc		hl
    ld		bc,0
    ld		(hl),c		; store x_l
    inc		hl
    ld		(hl),b		; store x_h
    inc		hl
    ld		bc,110
    ld		(hl),c		; store y_l
    inc		hl
    ld		(hl),b		; store y_h
    inc		hl
    ld		bc,SKY_OFFSET
    ld		(hl),c		; store offset_l
    inc		hl
    ld		(hl),b		; store offset_h
    inc		hl
    ; 1,2 sky
	ld		bc,$364F
    ld		(hl),c		; store width
    inc		hl
    ld		(hl),b		; store height
    inc		hl
    ld		bc,80
    ld		(hl),c		; store x_l
    inc		hl
    ld		(hl),b		; store x_h
    inc		hl
    ld		bc,110
    ld		(hl),c		; store y_l
    inc		hl
    ld		(hl),b		; store y_h
    inc		hl
    ld		bc,SKY_OFFSET
    ld		(hl),c		; store offset_l
    inc		hl
    ld		(hl),b		; store offset_h
    inc		hl
    ; 2,2 sky
	ld		bc,$364F
    ld		(hl),c		; store width
    inc		hl
    ld		(hl),b		; store height
    inc		hl
    ld		bc,160
    ld		(hl),c		; store x_l
    inc		hl
    ld		(hl),b		; store x_h
    inc		hl
    ld		bc,110
    ld		(hl),c		; store y_l
    inc		hl
    ld		(hl),b		; store y_h
    inc		hl
    ld		bc,SKY_OFFSET
    ld		(hl),c		; store offset_l
    inc		hl
    ld		(hl),b		; store offset_h
    inc		hl
    ; 3,2 sky
	ld		bc,$364F
    ld		(hl),c		; store width
    inc		hl
    ld		(hl),b		; store height
    inc		hl
    ld		bc,240
    ld		(hl),c		; store x_l
    inc		hl
    ld		(hl),b		; store x_h
    inc		hl
    ld		bc,110
    ld		(hl),c		; store y_l
    inc		hl
    ld		(hl),b		; store y_h
    inc		hl
    ld		bc,SKY_OFFSET
    ld		(hl),c		; store offset_l
    inc		hl
    ld		(hl),b		; store offset_h
    inc		hl
    ; 0 cities
    ld		bc,$3B8F
    ld		(hl),c		; store width
    inc		hl
    ld		(hl),b		; store height
    inc		hl
    ld		bc,0
    ld		(hl),c		; store x_l
    inc		hl
    ld		(hl),b		; store x_h
    inc		hl
    ld		bc,160
    ld		(hl),c		; store y_l
    inc		hl
    ld		(hl),b		; store y_h
    inc		hl
    ld		bc,CITY_OFFSET
    ld		(hl),c		; store offset_l
    inc		hl
    ld		(hl),b		; store offset_h
    inc		hl
    ; 1 cities
    ld		bc,$3B8F
    ld		(hl),c		; store width
    inc		hl
    ld		(hl),b		; store height
    inc		hl
    ld		bc,144
    ld		(hl),c		; store x_l
    inc		hl
    ld		(hl),b		; store x_h
    inc		hl
    ld		bc,160
    ld		(hl),c		; store y_l
    inc		hl
    ld		(hl),b		; store y_h
    inc		hl
    ld		bc,CITY_OFFSET
    ld		(hl),c		; store offset_l
    inc		hl
    ld		(hl),b		; store offset_h
    inc		hl
    ; 2 cities
    ld		bc,$3B8F
    ld		(hl),c		; store width
    inc		hl
    ld		(hl),b		; store height
    inc		hl
    ld		bc,288
    ld		(hl),c		; store x_l
    inc		hl
    ld		(hl),b		; store x_h
    inc		hl
    ld		bc,160
    ld		(hl),c		; store y_l
    inc		hl
    ld		(hl),b		; store y_h
    inc		hl
    ld		bc,CITY_OFFSET
    ld		(hl),c		; store offset_l
    inc		hl
    ld		(hl),b		; store offset_h
    inc		hl
    ld		(SPRITE_MEM),hl
    ret