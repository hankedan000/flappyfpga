----------------------------------------------------------------------------
-- Entity:        Project_djh5533_reb5427_tb
-- Written By:    Daniel Hankewycz & Robby Brague
-- Date Created:  10/28/2014
-- Description:   VHDL test bench for Project_djh5533_reb5427
--
-- Revision History (date, initials, description):
-- 
-- Dependencies:
--		Project_djh5533_reb5427
----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library HDL_Cmn;
use HDL_Cmn.Components.ALL;

----------------------------------------------------------------------------
entity Project_djh5533_reb5427_tb is
end    Project_djh5533_reb5427_tb;
----------------------------------------------------------------------------

architecture Behavioral of Project_djh5533_reb5427_tb is

	-- Unit Under Test (UUT)
	component Project_djh5533_reb5427 is
		 Port (	CLR		:	in		STD_LOGIC;
					CLK		:	in		STD_LOGIC;

					H_SYNC	:	out	STD_LOGIC;
					V_SYNC	:	out	STD_LOGIC;
					RGB		:	out	STD_LOGIC_VECTOR(11 downto 0);
					LED		: 	out 	STD_LOGIC_VECTOR(7 downto 0));	
	end component;

   --Inputs
	signal CLR		: STD_LOGIC					:= '0';
	signal CLK		: STD_LOGIC					:= '0';

 	--Outputs
	signal H_SYNC	: STD_LOGIC;
	signal V_SYNC	: STD_LOGIC;
	signal RGB		: STD_LOGIC_VECTOR (11 downto 0);
	signal LED		: STD_LOGIC_VECTOR (7 downto 0);
	
begin

	-- Instantiate the Unit Under Test (UUT)
	TOPLEVEL: Project_djh5533_reb5427 port map (CLR,CLK,H_SYNC,V_SYNC,RGB,LED);

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
		CLR	<= '1';
      wait for 20 ns;
		CLR	<= '0';
		
		wait;
		
   end process;
	
	-- CLOCK
	CLK <= not CLK after 5 ns; -- 100MHz clock

end Behavioral;

