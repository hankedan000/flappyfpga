----------------------------------------------------------------------------
-- Entity:        Counter_tb
-- Written By:    Daniel Hankewycz & Robby Brague
-- Date Created:  9/17/2014
-- Description:   VHDL test bench for Counter
--
-- Revision History (date, initials, description):
-- 
-- Dependencies:
-- 	Counter
--		Reg
----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library HDL_Cmn;
use HDL_Cmn.Components.ALL;

----------------------------------------------------------------------------
entity Gpu_Stage_One_tb is
end    Gpu_Stage_One_tb;
----------------------------------------------------------------------------

architecture Behavioral of Gpu_Stage_One_tb is

	-- Unit Under Test (UUT)
	component Gpu_Stage_One is
		 Port ( 	X_POS		:	in		STD_LOGIC_VECTOR (15 downto 0);
					Y_POS		:	in		STD_LOGIC_VECTOR (15 downto 0);
					WIDTH		:	in		STD_LOGIC_VECTOR (7 downto 0);
					HEIGHT	:	in		STD_LOGIC_VECTOR (7 downto 0);
					OFF		:	in		STD_LOGIC_VECTOR (15 downto 0);
					LOAD		:	in		STD_LOGIC;
					CLR		:	in		STD_LOGIC;
					CLK		:	in		STD_LOGIC;
					
					INVALID	:	out	STD_LOGIC;
					X_OUT		:	out	STD_LOGIC_VECTOR (15 downto 0);
					Y_OUT		:	out	STD_LOGIC_VECTOR (15 downto 0);
					W_OUT		:	out	STD_LOGIC_VECTOR (7 downto 0);
					H_OUT		:	out	STD_LOGIC_VECTOR (7 downto 0);
					OFF_OUT	:	out	STD_LOGIC_VECTOR (15 downto 0));
	end component;

   --Inputs
   signal X_POS		: STD_LOGIC_VECTOR (15 downto 0)	:= x"000A";
	signal Y_POS		: STD_LOGIC_VECTOR (15 downto 0)	:= x"000A";
	signal WIDTH		: STD_LOGIC_VECTOR (7 downto 0)	:= x"06";
	signal HEIGHT		: STD_LOGIC_VECTOR (7 downto 0)	:= x"04";
	signal OFF			: STD_LOGIC_VECTOR (15 downto 0)	:= x"00CD";
	signal LOAD			: STD_LOGIC	:= '0';
   signal CLR    		: STD_LOGIC	:= '0';
   signal CLK    		: STD_LOGIC	:= '0';

 	--Outputs
   signal INVALID		: STD_LOGIC;
   signal X_OUT		: STD_LOGIC_VECTOR (15 downto 0);
   signal Y_OUT		: STD_LOGIC_VECTOR (15 downto 0);
   signal W_OUT		: STD_LOGIC_VECTOR (7 downto 0);
   signal H_OUT		: STD_LOGIC_VECTOR (7 downto 0);
   signal OFF_OUT		: STD_LOGIC_VECTOR (15 downto 0);
	
begin

	-- Instantiate the Unit Under Test (UUT)
   IMGPOS: Gpu_Stage_One port map (X_POS,Y_POS,WIDTH,HEIGHT,OFF,LOAD,CLR,CLK,INVALID,X_OUT,Y_OUT,W_OUT,H_OUT,OFF_OUT);

   -- Stimulus process
   stim_proc: process
   begin		
		CLR <= '1';
		wait for 10 ns;
		CLR <= '0';
		wait for 20 ns;
		-- Load in sprite 1
		LOAD <= '1';	wait for 10 ns;	LOAD <= '0';
		
		-- Send sprite 2 data
		wait until invalid='1';
		WIDTH		<= x"10";
		HEIGHT	<= x"0B";
		X_POS 	<= x"0002";
		Y_POS 	<= x"0002";
		OFF		<= x"0030";
		-- Load sprite 2
		LOAD <= '1';	wait for 10 ns;	LOAD <= '0';
		
		-- Send sprite 3 data
		wait until invalid='1';
		WIDTH		<= x"00";
		HEIGHT	<= x"00";
		X_POS 	<= x"0002";
		Y_POS 	<= x"0002";
		OFF		<= x"0010";
		-- Load sprite 3
		LOAD <= '1';	wait for 10 ns;	LOAD <= '0';
		
		
		
      wait;
		
   end process;
	
	CLK	<= not CLK after 5 ns;

end Behavioral;

