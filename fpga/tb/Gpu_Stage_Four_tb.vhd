----------------------------------------------------------------------------
-- Entity:        Gpu_Stage_Three_tb
-- Written By:    Daniel Hankewycz & Robby Brague
-- Date Created:  9/17/2014
-- Description:   VHDL test bench for Counter
--
-- Revision History (date, initials, description):
-- 
-- Dependencies:
-- 	Counter
--		Reg
----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library HDL_Cmn;
use HDL_Cmn.Components.ALL;

----------------------------------------------------------------------------
entity Gpu_Stage_Four_tb is
end    Gpu_Stage_Four_tb;
----------------------------------------------------------------------------

architecture Behavioral of Gpu_Stage_Four_tb is

	-- Unit Under Test (UUT)
	component Gpu_Stage_Four is
		 Port ( 	RAM_ADDRESS	:	in		STD_LOGIC_VECTOR (19 downto 0);
					PIX_COLOR	:	in		STD_LOGIC_VECTOR (7 downto 0);
					BUFF_STAGE	:	in		STD_LOGIC;
					W_EN			:	in		STD_LOGIC;
					CLR			:	in		STD_LOGIC;
					CLK			:	in		STD_LOGIC;
					
					V_SYNC		: 	out	STD_LOGIC;
					H_SYNC		: 	out	STD_LOGIC;
					RGB_OUT		: 	out	STD_LOGIC_VECTOR(11 downto 0));
	end component;
	
   --Inputs
   signal RAM_ADDRESS	: STD_LOGIC_VECTOR (19 downto 0)	:= x"00002";
	signal PIX_COLOR		: STD_LOGIC_VECTOR (7 downto 0)	:= x"CF";
	signal BUFF_STAGE		: STD_LOGIC := '0';
	signal W_EN				: STD_LOGIC	:= '1';
	signal CLR				: STD_LOGIC	:= '1';
   signal CLK    			: STD_LOGIC	:= '0';

 	--Outputs
   signal V_SYNC			: STD_LOGIC;
   signal H_SYNC			: STD_LOGIC;
   signal RGB_OUT			: STD_LOGIC_VECTOR (11 downto 0);
	
begin

	-- Instantiate the Unit Under Test (UUT)
   STGFOUR: Gpu_Stage_Four port map (RAM_ADDRESS,PIX_COLOR,BUFF_STAGE,W_EN,CLR,CLK,V_SYNC,H_SYNC,RGB_OUT);

   -- Stimulus process
   stim_proc: process
   begin		
		-- Initial CLR
		CLR <= '1';	wait until rising_edge(CLK);	CLR <= '0';
		wait for 50 ns;
		PIX_COLOR <= x"0F";
		wait until rising_edge(CLK);
      wait;
		
   end process;
	
	CLK	<= not CLK after 5 ns;

end Behavioral;

