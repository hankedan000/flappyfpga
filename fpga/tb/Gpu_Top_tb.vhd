----------------------------------------------------------------------------
-- Entity:        Gpu_Stage_Three_tb
-- Written By:    Daniel Hankewycz & Robby Brague
-- Date Created:  9/17/2014
-- Description:   VHDL test bench for Counter
--
-- Revision History (date, initials, description):
-- 
-- Dependencies:
-- 	Counter
--		Reg
----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library HDL_Cmn;
use HDL_Cmn.Components.ALL;

----------------------------------------------------------------------------
entity Gpu_Top_tb is
end    Gpu_Top_tb;
----------------------------------------------------------------------------

architecture Behavioral of Gpu_Top_tb is

	-- Unit Under Test (UUT)
	component Gpu_Top is
		 Port ( 	SPRITE_DATA	:	in		STD_LOGIC_VECTOR(7 downto 0);
					DB_STAT		:	in		STD_LOGIC_VECTOR(7 downto 0);
					LD_STAT		:	in		STD_LOGIC;
					DISABLE		:	in		STD_LOGIC;
					BUTTON		:	in		STD_LOGIC;
					CLR			:	in		STD_LOGIC;
					CLK			:	in		STD_LOGIC;
					
					V_SYNC		: 	out	STD_LOGIC;
					H_SYNC		: 	out	STD_LOGIC;
					SPRITE_ADDR	: 	out	STD_LOGIC_VECTOR(10 downto 0);
					SCREEN_DONE	: 	out	STD_LOGIC_VECTOR(7 downto 0);
					RGB_OUT		: 	out	STD_LOGIC_VECTOR(11 downto 0));
	end component;
	
   --Inputs
   signal SPRITE_DATA	: STD_LOGIC_VECTOR (7 downto 0)	:= x"02";
	signal DB_STAT			: STD_LOGIC_VECTOR (7 downto 0)	:= x"CF";
	signal LD_STAT			: STD_LOGIC := '0';
	signal DISABLE			: STD_LOGIC := '0';
	signal BUTTON			: STD_LOGIC := '0';
	signal CLR				: STD_LOGIC	:= '1';
   signal CLK    			: STD_LOGIC	:= '0';

 	--Outputs
   signal V_SYNC			: STD_LOGIC;
   signal H_SYNC			: STD_LOGIC;
	signal SPRITE_ADDR	: STD_LOGIC_VECTOR (10 downto 0);
	signal SCREEN_DONE	: STD_LOGIC_VECTOR (7 downto 0);
   signal RGB_OUT			: STD_LOGIC_VECTOR (11 downto 0);
	
begin

	-- Instantiate the Unit Under Test (UUT)
   uut: Gpu_Top port map (SPRITE_DATA,DB_STAT,LD_STAT,DISABLE,BUTTON,CLR,CLK,V_SYNC,H_SYNC,SPRITE_ADDR,SCREEN_DONE,RGB_OUT);

   -- Stimulus process
   stim_proc: process
   begin		
		-- Initial CLR
		CLR <= '1';	wait until rising_edge(CLK);	CLR <= '0';
		-- sprite 1
		wait until SPRITE_ADDR="00000000000" and rising_edge(CLK);	SPRITE_DATA<=x"10";	-- WIDTH
		wait until SPRITE_ADDR="00000000001" and rising_edge(CLK);	SPRITE_DATA<=x"0B";	-- HEIGHT
		wait until SPRITE_ADDR="00000000010" and rising_edge(CLK);	SPRITE_DATA<=x"0A";	-- X_L
		wait until SPRITE_ADDR="00000000011" and rising_edge(CLK);	SPRITE_DATA<=x"00";	-- X_H
		wait until SPRITE_ADDR="00000000100" and rising_edge(CLK);	SPRITE_DATA<=x"0A";	-- Y_L
		wait until SPRITE_ADDR="00000000101" and rising_edge(CLK);	SPRITE_DATA<=x"00";	-- Y_H
		wait until SPRITE_ADDR="00000000110" and rising_edge(CLK);	SPRITE_DATA<=x"00";	-- OFF_L
		wait until SPRITE_ADDR="00000000111" and rising_edge(CLK);	SPRITE_DATA<=x"00";	-- OFF_H
		
--		-- sprite 3
--		wait until SPRITE_ADDR="1011111000" and rising_edge(CLK);	SPRITE_DATA<=x"17";	-- WIDTH
--		wait until SPRITE_ADDR="1011111001" and rising_edge(CLK);	SPRITE_DATA<=x"13";	-- HEIGHT
--		wait until SPRITE_ADDR="1011111010" and rising_edge(CLK);	SPRITE_DATA<=x"00";	-- X_L
--		wait until SPRITE_ADDR="1011111011" and rising_edge(CLK);	SPRITE_DATA<=x"00";	-- X_H
--		wait until SPRITE_ADDR="1011111100" and rising_edge(CLK);	SPRITE_DATA<=x"00";	-- Y_L
--		wait until SPRITE_ADDR="1011111101" and rising_edge(CLK);	SPRITE_DATA<=x"00";	-- Y_H
--		wait until SPRITE_ADDR="1011111110" and rising_edge(CLK);	SPRITE_DATA<=x"0c";	-- OFF_L
--		wait until SPRITE_ADDR="1011111111" and rising_edge(CLK);	SPRITE_DATA<=x"0f";	-- OFF_H
--		
--		-- sprite 3
--		wait until SPRITE_ADDR="1100001000" and rising_edge(CLK);	SPRITE_DATA<=x"17";	-- WIDTH
--		wait until SPRITE_ADDR="1100001001" and rising_edge(CLK);	SPRITE_DATA<=x"13";	-- HEIGHT
--		wait until SPRITE_ADDR="1100001010" and rising_edge(CLK);	SPRITE_DATA<=x"00";	-- X_L
--		wait until SPRITE_ADDR="1100001011" and rising_edge(CLK);	SPRITE_DATA<=x"00";	-- X_H
--		wait until SPRITE_ADDR="1100001100" and rising_edge(CLK);	SPRITE_DATA<=x"00";	-- Y_L
--		wait until SPRITE_ADDR="1100001101" and rising_edge(CLK);	SPRITE_DATA<=x"00";	-- Y_H
--		wait until SPRITE_ADDR="1100001110" and rising_edge(CLK);	SPRITE_DATA<=x"0c";	-- OFF_L
--		wait until SPRITE_ADDR="1100001111" and rising_edge(CLK);	SPRITE_DATA<=x"0f";	-- OFF_H
		SPRITE_DATA<=x"00";
		
		wait until SCREEN_DONE(0)='1';
		DB_STAT <= x"01";
		LD_STAT <= '1';
		wait until rising_edge(CLK);
		LD_STAT <= '0';
		wait until rising_edge(CLK);
		wait until SCREEN_DONE(0)='1';
		DB_STAT <= x"00";
		LD_STAT <= '1';
		wait until rising_edge(CLK);
		LD_STAT <= '0';
		wait until rising_edge(CLK);
		wait until SCREEN_DONE(0)='1';
		DB_STAT <= x"01";
		LD_STAT <= '1';
		wait until rising_edge(CLK);
		LD_STAT <= '0';
		wait until rising_edge(CLK);
		wait until SCREEN_DONE(0)='1';
		DB_STAT <= x"00";
		LD_STAT <= '1';
		wait until rising_edge(CLK);
		LD_STAT <= '0';
		
      wait;
		
   end process;
	
	CLK	<= not CLK after 5 ns;

end Behavioral;

