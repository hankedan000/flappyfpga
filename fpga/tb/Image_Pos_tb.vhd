----------------------------------------------------------------------------
-- Entity:        Counter_tb
-- Written By:    Daniel Hankewycz & Robby Brague
-- Date Created:  9/17/2014
-- Description:   VHDL test bench for Counter
--
-- Revision History (date, initials, description):
-- 
-- Dependencies:
-- 	Counter
--		Reg
----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library HDL_Cmn;
use HDL_Cmn.Components.ALL;

----------------------------------------------------------------------------
entity Image_Pos_tb is
end    Image_Pos_tb;
----------------------------------------------------------------------------

architecture Behavioral of Image_Pos_tb is

	-- Unit Under Test (UUT)
	component Image_Pos is
		Port ( 	WIDTH		:	in		STD_LOGIC_VECTOR (7 downto 0);
					HEIGHT	:	in		STD_LOGIC_VECTOR (7 downto 0);
					CLR		:	in		STD_LOGIC;
					CLK		:	in		STD_LOGIC;
					
					Q			:	out	STD_LOGIC;
					W_OUT		:	out	STD_LOGIC_VECTOR (7 downto 0);
					H_OUT		:	out	STD_LOGIC_VECTOR (7 downto 0));
	end component;

   --Inputs
   signal WIDTH		: STD_LOGIC_VECTOR (7 downto 0)	:= x"05";
	signal HEIGHT		: STD_LOGIC_VECTOR (7 downto 0)	:= x"05";
   signal CLR    		: STD_LOGIC	:= '0';
   signal CLK    		: STD_LOGIC	:= '0';

 	--Outputs
   signal Q		 		: STD_LOGIC;
   signal W_OUT		: STD_LOGIC_VECTOR (7 downto 0);
   signal H_OUT		: STD_LOGIC_VECTOR (7 downto 0);
	
begin

	-- Instantiate the Unit Under Test (UUT)
   IMGPOS: Image_Pos port map (WIDTH,HEIGHT,CLR,CLK,Q,W_OUT,H_OUT);

   -- Stimulus process
   stim_proc: process
   begin		
		CLR <= '1';
		wait for 100 ns;
		CLR <= '0';
		wait for 1000 ns;
		CLR <= '1';
		wait for 100 ns;
		CLR <= '0';
		wait for 500 ns;
		
      wait;
		
   end process;
	
	CLK	<= not CLK after 10 ns;

end Behavioral;

