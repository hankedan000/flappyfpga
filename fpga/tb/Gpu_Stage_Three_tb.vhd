----------------------------------------------------------------------------
-- Entity:        Gpu_Stage_Three_tb
-- Written By:    Daniel Hankewycz & Robby Brague
-- Date Created:  9/17/2014
-- Description:   VHDL test bench for Counter
--
-- Revision History (date, initials, description):
-- 
-- Dependencies:
-- 	Counter
--		Reg
----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library HDL_Cmn;
use HDL_Cmn.Components.ALL;

----------------------------------------------------------------------------
entity Gpu_Stage_Three_tb is
end    Gpu_Stage_Three_tb;
----------------------------------------------------------------------------

architecture Behavioral of Gpu_Stage_Three_tb is

	-- Unit Under Test (UUT)
	component Gpu_Stage_Three is
		 Port ( 	START_ADD	:	in		STD_LOGIC_VECTOR (19 downto 0);
					IMAGE_ADD	:	in		STD_LOGIC_VECTOR (19 downto 0);
					IMAGE_OFF	:	in		STD_LOGIC_VECTOR (15 downto 0);
					PIX_OFF		:	in		STD_LOGIC;
					CLR			:	in		STD_LOGIC;
					CLK			:	in		STD_LOGIC;
					
					W_EN			:	out	STD_LOGIC;
					RAM_ADDRESS	:	out	STD_LOGIC_VECTOR (19 downto 0);
					PIX_COLOR	:	out	STD_LOGIC_VECTOR (7 downto 0));
	end component;
   --Inputs
   signal START_ADD		: STD_LOGIC_VECTOR (19 downto 0)	:= x"00002";
	signal IMAGE_ADD		: STD_LOGIC_VECTOR (19 downto 0)	:= x"00003";
	signal IMAGE_OFF		: STD_LOGIC_VECTOR (15 downto 0)	:= x"0140";
	signal PIX_OFF			: STD_LOGIC	:= '0';
	signal CLR				: STD_LOGIC := '0';
   signal CLK    			: STD_LOGIC	:= '0';

 	--Outputs
   signal W_EN				: STD_LOGIC;
   signal RAM_ADDRESS	: STD_LOGIC_VECTOR (19 downto 0);
   signal PIX_COLOR		: STD_LOGIC_VECTOR (7 downto 0);
	
begin

	-- Instantiate the Unit Under Test (UUT)
   STGTHREE: Gpu_Stage_Three port map (START_ADD,IMAGE_ADD,IMAGE_OFF,PIX_OFF,CLR,CLK,W_EN,RAM_ADDRESS,PIX_COLOR);

   -- Stimulus process
   stim_proc: process
   begin		
		-- Initial Clear
		CLR <= '1'; wait until rising_edge(CLK); CLR <= '0';
	
		PIX_OFF <= '0';
		wait until rising_edge(CLK);
		START_ADD	<= x"00002";
		IMAGE_ADD	<= x"00004";
		IMAGE_OFF	<= x"000A";
		
		PIX_OFF <= '1';
		wait until rising_edge(CLK);
		START_ADD	<= x"00002";
		IMAGE_ADD	<= x"00004";
		IMAGE_OFF	<= x"000A";
		
      wait;
		
   end process;
	
	CLK	<= not CLK after 5 ns;

end Behavioral;

