----------------------------------------------------------------------------
-- Entity:        Counter_tb
-- Written By:    Daniel Hankewycz & Robby Brague
-- Date Created:  9/17/2014
-- Description:   VHDL test bench for Counter
--
-- Revision History (date, initials, description):
-- 
-- Dependencies:
-- 	Counter
--		Reg
----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library HDL_Cmn;
use HDL_Cmn.Components.ALL;

----------------------------------------------------------------------------
entity Gpu_Stage_Two_tb is
end    Gpu_Stage_Two_tb;
----------------------------------------------------------------------------

architecture Behavioral of Gpu_Stage_Two_tb is

	-- Unit Under Test (UUT)
	component Gpu_Stage_Two is
		 Port ( 	X_IN			:	in		STD_LOGIC_VECTOR (15 downto 0);
					Y_IN			:	in		STD_LOGIC_VECTOR (15 downto 0);
					W_IN			:	in		STD_LOGIC_VECTOR (7 downto 0);
					H_IN			:	in		STD_LOGIC_VECTOR (7 downto 0);
					OFF_IN		:	in		STD_LOGIC_VECTOR (15 downto 0);
					INVALID		:	in		STD_LOGIC;
					CLR			:	in		STD_LOGIC;
					CLK			:	in		STD_LOGIC;
					
					PIX_OFF		:	out	STD_LOGIC;
					START_ADD	:	out	STD_LOGIC_VECTOR (19 downto 0);
					IMAGE_ADD	:	out	STD_LOGIC_VECTOR (19 downto 0);
					IMAGE_OFF	:	out	STD_LOGIC_VECTOR (15 downto 0));
	end component;

   --Inputs
   signal X_IN			: STD_LOGIC_VECTOR (15 downto 0)	:= x"0002";
	signal Y_IN			: STD_LOGIC_VECTOR (15 downto 0)	:= x"0006";
	signal W_IN			: STD_LOGIC_VECTOR (7 downto 0)	:= x"10";
	signal H_IN			: STD_LOGIC_VECTOR (7 downto 0)	:= x"0B";
	signal OFF_IN		: STD_LOGIC_VECTOR (15 downto 0)	:= x"0030";
   signal INVALID    : STD_LOGIC	:= '0';
	signal CLR			: STD_LOGIC := '0';
   signal CLK    		: STD_LOGIC	:= '0';

 	--Outputs
   signal PIX_OFF		: STD_LOGIC;
   signal START_ADD	: STD_LOGIC_VECTOR (19 downto 0);
   signal IMAGE_ADD	: STD_LOGIC_VECTOR (19 downto 0);
   signal IMAGE_OFF	: STD_LOGIC_VECTOR (15 downto 0);
	
begin

	-- Instantiate the Unit Under Test (UUT)
   STGTWO: Gpu_Stage_Two port map (X_IN,Y_IN,W_IN,H_IN,OFF_IN,INVALID,CLR,CLK,PIX_OFF,START_ADD,IMAGE_ADD,IMAGE_OFF);

   -- Stimulus process
   stim_proc: process
   begin		
		-- Initial clear
		CLR <= '1'; wait for 10 ns; CLR <= '0';
		
		INVALID <= '0';
		wait until rising_edge(CLK);
		X_IN <= x"0003";
		Y_IN <= x"0006";
		W_IN <= x"10";
		H_IN <= x"0B";
		wait until rising_edge(CLK);
		X_IN <= x"0003";
		Y_IN <= x"0007";
		W_IN <= x"00";
		H_IN <= x"00";
		wait until rising_edge(CLK);
		X_IN <= x"FFFF";
		Y_IN <= x"0007";
		W_IN <= x"00";
		H_IN <= x"00";
		wait until rising_edge(CLK);
		X_IN <= x"0000";
		Y_IN <= x"0000";
		W_IN <= x"00";
		H_IN <= x"00";
		wait until rising_edge(CLK);
		X_IN <= x"0000";
		Y_IN <= x"FFFF";
		W_IN <= x"00";
		H_IN <= x"00";
		wait until rising_edge(CLK);
		X_IN <= x"013F";
		Y_IN <= x"0000";
		W_IN <= x"00";
		H_IN <= x"00";
		wait until rising_edge(CLK);
		X_IN <= x"0140";
		Y_IN <= x"0000";
		W_IN <= x"00";
		H_IN <= x"00";
		wait until rising_edge(CLK);
		X_IN <= x"0000";
		Y_IN <= x"00EF";
		W_IN <= x"00";
		H_IN <= x"00";
		wait until rising_edge(CLK);
		X_IN <= x"0000";
		Y_IN <= x"00F0";
		W_IN <= x"00";
		H_IN <= x"00";
		wait until rising_edge(CLK);
		X_IN <= x"0020";
		Y_IN <= x"0010";
		W_IN <= x"01";
		H_IN <= x"02";
		wait until rising_edge(CLK);
		INVALID <= '1';
      wait;
		
   end process;
	
	CLK	<= not CLK after 5 ns;

end Behavioral;

