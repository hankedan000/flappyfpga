----------------------------------------------------------------------------
-- Entity:        Pulse_Gen_Cnt_Out
-- Written By:    Daniel Hankewycz & Robby Brague
-- Date Created:  9/18/2014
-- Description:   Creates an nbit Pulse_Gen_Cnt_Out
--
-- Revision History (date, initials, description):
-- 
-- Dependencies:
--		Counter
--		CompareEQU
----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library HDL_Cmn;
use HDL_Cmn.Components.ALL;

entity Image_Pos is
    Port ( 	WIDTH		:	in		STD_LOGIC_VECTOR (7 downto 0);
				HEIGHT	:	in		STD_LOGIC_VECTOR (7 downto 0);
				CLR		:	in		STD_LOGIC;
				CLK		:	in		STD_LOGIC;
				
				Q			:	out	STD_LOGIC;
				W_OUT		:	out	STD_LOGIC_VECTOR (7 downto 0);
				H_OUT		:	out	STD_LOGIC_VECTOR (7 downto 0));
end Image_Pos;

architecture dataflow of Image_Pos is

signal	done, x_q, y_q, drawing_last_line	:	STD_LOGIC;
signal	x_cnt, y_cnt	: STD_LOGIC_VECTOR(7 downto 0);
signal	int_equ	:	STD_LOGIC;

begin
	XPOS	:	Pulse_Gen_Cnt_Out generic map(n=>8) port map(
		CNT		=> WIDTH,
		CLR		=> CLR or x_q,
		EN			=> '1',
		CLK		=> CLK,
		
		Q			=> x_q,
		CNT_OUT	=> x_cnt);
	
	YPOS	:	Pulse_Gen_Cnt_Out generic map(n=>8) port map(
		CNT		=> HEIGHT,
		CLR		=> CLR,
		EN			=> x_q,
		CLK		=> CLK,
		
		Q			=> y_q,
		CNT_OUT	=> y_cnt);
	
	done	<= '1' when unsigned(y_cnt)=(unsigned(HEIGHT)+x"01") and x_cnt=x"00" else
				'0';
	W_OUT	<= x_cnt;
	H_OUT	<= y_cnt;
	Q		<= done;
end dataflow;