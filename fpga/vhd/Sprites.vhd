-- This file was generated with hex2rom written by Daniel Wallner

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity Sprites is
	port(
		CE_n	: in std_logic;
		OE_n	: in std_logic;
		A		: in std_logic_vector(9 downto 0);
		D		: out std_logic_vector(7 downto 0));
end Sprites;

architecture rtl of Sprites is
	subtype ROM_WORD is std_logic_vector(7 downto 0);
	type ROM_TABLE is array(0 to (2**10)-1) of ROM_WORD;
	constant ROM: ROM_TABLE := ROM_TABLE'(
		--SQUARE 1--
		x"10",				--WIDTH
		x"0B",				--HEIGHT
		x"00",				--X_L
		x"00",				--X_H
		x"00",				--Y_L
		x"00",				--Y_H
		x"00",				--OFF_L
		x"00",				--OFF_H
		--SQUARE 2--
		x"10",				--WIDTH
		x"0B",				--HEIGHT
		x"11",				--X_L
		x"00",				--X_H
		x"00",				--Y_L
		x"00",				--Y_H
		x"11",				--OFF_L
		x"00",				--OFF_H
		--SQUARE 3--
		x"10",				--WIDTH
		x"0B",				--HEIGHT
		x"22",				--X_L
		x"00",				--X_H
		x"00",				--Y_L
		x"00",				--Y_H
		x"22",				--OFF_L
		x"00",				--OFF_H
		--SQUARE 4--
		x"FE",				--WIDTH
		x"77",				--HEIGHT
		x"00",				--X_L
		x"00",				--X_H
		x"20",				--Y_L
		x"00",				--Y_H
		x"00",				--OFF_L
		x"00",				--OFF_H
		--SQUARE 5--
		x"00",				--WIDTH
		x"00",				--HEIGHT
		x"00",				--X_L
		x"00",				--X_H
		x"00",				--Y_L
		x"00",				--Y_H
		x"00",				--OFF_L
		x"00",				--OFF_H
		others => x"00");
		
begin
	D <= ROM(to_integer(unsigned(A))) when CE_n = '0' and OE_n = '0' else (others => 'Z');
end;
