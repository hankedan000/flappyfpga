----------------------------------------------------------------------------
-- Entity:        Gpu_Stage_Four
-- Written By:    Daniel Hankewycz & Robby Brague
-- Date Created:  12/04/2014
-- Description:   First stage for gpu to track image location
--
-- Revision History (date, initials, description):
-- 
-- Dependencies:
--		Reg
--		Image_Pos
----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library HDL_Cmn;
use HDL_Cmn.Components.ALL;

entity Gpu_Top is
    Port ( 	SPRITE_DATA	:	in		STD_LOGIC_VECTOR(7 downto 0);
				DB_STAT		:	in		STD_LOGIC_VECTOR(7 downto 0);
				LD_STAT		:	in		STD_LOGIC;
				DISABLE		:	in		STD_LOGIC;
				BUTTON		:	in		STD_LOGIC;
				CLR			:	in		STD_LOGIC;
				CLK			:	in		STD_LOGIC;
				
				V_SYNC		: 	out	STD_LOGIC;
				H_SYNC		: 	out	STD_LOGIC;
				SPRITE_ADDR	: 	out	STD_LOGIC_VECTOR(10 downto 0);
				SCREEN_DONE	: 	out	STD_LOGIC_VECTOR(7 downto 0);
				RGB_OUT		: 	out	STD_LOGIC_VECTOR(11 downto 0));
end Gpu_Top;
architecture dataflow of Gpu_Top is
component Gpu_Stage_Zero is
    Port ( 	NEXT_SPRITE	: in 	STD_LOGIC;
				SPRITE_DATA	: in	STD_LOGIC_VECTOR(7 downto 0);
				BUFF_STAGE	: in	STD_LOGIC;
				CLK			: in	STD_LOGIC;
				CLR			: in 	STD_LOGIC;
				
				SPRITE_ADDR	: out STD_LOGIC_VECTOR(10 downto 0);
				X				: out STD_LOGIC_VECTOR(15 downto 0);
				Y				: out STD_LOGIC_VECTOR(15 downto 0);
				WIDTH			: out STD_LOGIC_VECTOR(7 downto 0);
				HEIGHT		: out STD_LOGIC_VECTOR(7 downto 0);
				SHEET_OFFSET: out STD_LOGIC_VECTOR(15 downto 0);
				DONE			: out STD_LOGIC;
				READY			: out STD_LOGIC);
end component;

component Gpu_Stage_One is
    Port ( 	X_POS		:	in		STD_LOGIC_VECTOR (15 downto 0);
				Y_POS		:	in		STD_LOGIC_VECTOR (15 downto 0);
				WIDTH		:	in		STD_LOGIC_VECTOR (7 downto 0);
				HEIGHT	:	in		STD_LOGIC_VECTOR (7 downto 0);
				OFF		:	in		STD_LOGIC_VECTOR (15 downto 0);
				LOAD		:	in		STD_LOGIC;
				CLR		:	in		STD_LOGIC;
				CLK		:	in		STD_LOGIC;
				
				INVALID	:	out	STD_LOGIC;
				X_OUT		:	out	STD_LOGIC_VECTOR (15 downto 0);
				Y_OUT		:	out	STD_LOGIC_VECTOR (15 downto 0);
				W_OUT		:	out	STD_LOGIC_VECTOR (7 downto 0);
				H_OUT		:	out	STD_LOGIC_VECTOR (7 downto 0);
				OFF_OUT	:	out	STD_LOGIC_VECTOR (15 downto 0));
end component;

component Gpu_Stage_Two is
	 Port ( 	X_IN			:	in		STD_LOGIC_VECTOR (15 downto 0);
				Y_IN			:	in		STD_LOGIC_VECTOR (15 downto 0);
				W_IN			:	in		STD_LOGIC_VECTOR (7 downto 0);
				H_IN			:	in		STD_LOGIC_VECTOR (7 downto 0);
				OFF_IN		:	in		STD_LOGIC_VECTOR (15 downto 0);
				INVALID		:	in		STD_LOGIC;
				CLR			:	in		STD_LOGIC;
				CLK			:	in		STD_LOGIC;
				
				PIX_OFF		:	out	STD_LOGIC;
				START_ADD	:	out	STD_LOGIC_VECTOR (19 downto 0);
				IMAGE_ADD	:	out	STD_LOGIC_VECTOR (19 downto 0);
				IMAGE_OFF	:	out	STD_LOGIC_VECTOR (15 downto 0));
end component;

component Gpu_Stage_Three is
	 Port ( 	START_ADD	:	in		STD_LOGIC_VECTOR (19 downto 0);
				IMAGE_ADD	:	in		STD_LOGIC_VECTOR (19 downto 0);
				IMAGE_OFF	:	in		STD_LOGIC_VECTOR (15 downto 0);
				PIX_OFF		:	in		STD_LOGIC;
				CLR			:	in		STD_LOGIC;
				CLK			:	in		STD_LOGIC;
				
				W_EN			:	out	STD_LOGIC;
				RAM_ADDRESS	:	out	STD_LOGIC_VECTOR (19 downto 0);
				PIX_COLOR	:	out	STD_LOGIC_VECTOR (7 downto 0));
end component;

component Gpu_Stage_Four is
	 Port ( 	RAM_ADDRESS	:	in		STD_LOGIC_VECTOR (19 downto 0);
				PIX_COLOR	:	in		STD_LOGIC_VECTOR (7 downto 0);
				BUFF_STAGE	:	in		STD_LOGIC;
				W_EN			:	in		STD_LOGIC;
				CLR			:	in		STD_LOGIC;
				CLK			:	in		STD_LOGIC;
				
				V_SYNC		: 	out	STD_LOGIC;
				H_SYNC		: 	out	STD_LOGIC;
				RGB_OUT		: 	out	STD_LOGIC_VECTOR(11 downto 0));
end component;

component Button_Catch is
    Port ( 	BUTTON	:	in		STD_LOGIC_VECTOR(7 downto 0);
				CLR		:	in		STD_LOGIC;
				CLK		:	in		STD_LOGIC;
				
				Q			:	out	STD_LOGIC_VECTOR(7 downto 0));
end component;
--transfer zero--
signal	READY			:	STD_LOGIC;
signal	X_POS			:	STD_LOGIC_VECTOR (15 downto 0);
signal	Y_POS			:	STD_LOGIC_VECTOR (15 downto 0);
signal	WIDTH			:	STD_LOGIC_VECTOR (7 downto 0);
signal	HEIGHT		:	STD_LOGIC_VECTOR (7 downto 0);
signal	OFF			:	STD_LOGIC_VECTOR (15 downto 0);
--transfer one
signal	INVALID		:	STD_LOGIC;
signal	X_OUT			:	STD_LOGIC_VECTOR (15 downto 0);
signal	Y_OUT			:	STD_LOGIC_VECTOR (15 downto 0);
signal	W_OUT			:	STD_LOGIC_VECTOR (7 downto 0);
signal	H_OUT			:	STD_LOGIC_VECTOR (7 downto 0);
signal	OFF_OUT		:	STD_LOGIC_VECTOR (15 downto 0);
--transfer two
signal	PIX_OFF		:	STD_LOGIC;
signal	START_ADD	:	STD_LOGIC_VECTOR (19 downto 0);
signal	IMAGE_ADD	:	STD_LOGIC_VECTOR (19 downto 0);
signal	IMAGE_OFF	:	STD_LOGIC_VECTOR (15 downto 0);
--transfer three
signal	W_EN			:	STD_LOGIC;
signal	RAM_ADDRESS	:	STD_LOGIC_VECTOR (19 downto 0);
signal	PIX_COLOR	:	STD_LOGIC_VECTOR (7 downto 0);
--other signals--
signal	button_pressed	:	STD_LOGIC_VECTOR(7 downto 0);
signal	buff_stage	:	STD_LOGIC;
signal	change		:	STD_LOGIC;
signal	done			:	STD_LOGIC;
signal	gpu_done		:	STD_LOGIC;
signal	done_out		:	STD_LOGIC;
signal	next_frame	:	STD_LOGIC;
signal	v_sync_temp	: 	STD_LOGIC;


begin
	BUFFREG	:	Reg generic map(n=>1) port map(
		D(0)	=>	DB_STAT(0),
		LOAD	=>	LD_STAT,
		CLR	=>	CLR,
		CLK	=>	CLK,
		
		Q(0)	=>	buff_stage);
	
	NEXTFRAME	:	Button_Catch port map(
		BUTTON	=>	"0000000" & BUTTON,
		CLR		=> CLR OR change,
		CLK		=>	CLK,
		
		Q			=>	button_pressed);
	
	next_frame	<= '1' when (DISABLE = '0') OR (button_pressed(0) = '1') else '0'; 
	change	<=	'1' when (buff_stage /= DB_STAT(0)) AND (LD_STAT = '1') else '0';
	gpu_done	<=	done AND INVALID AND PIX_OFF AND NOT W_EN AND next_frame AND NOT v_sync_temp;
	
	DONEREG	:	Reg generic map(n=>1) port map(
		D(0)	=>	gpu_done,
		LOAD	=>	'1',
		CLR	=>	CLR,
		CLK	=>	CLK,
		
		Q(0)	=>	done_out);
	
	SCREEN_DONE	<= "00000" & buff_stage & change & done_out;
	
	STAGEZERO	:	Gpu_Stage_Zero port map(
		NEXT_SPRITE		=>	INVALID AND READY AND NOT done,
		SPRITE_DATA		=>	SPRITE_DATA,
		BUFF_STAGE		=>	buff_stage,
		CLK				=>	CLK,
		CLR				=>	CLR OR change,
		
		SPRITE_ADDR		=>	SPRITE_ADDR,
		X					=>	X_POS,
		Y					=>	Y_POS,
		WIDTH				=>	WIDTH,
		HEIGHT			=>	HEIGHT,
		SHEET_OFFSET	=>	OFF,
		DONE				=>	done,
		READY				=>	READY);
	
	STAGEONE	:	Gpu_Stage_One port map(
		X_POS		=>	X_POS,
		Y_POS		=>	Y_POS,
		WIDTH		=>	WIDTH,
		HEIGHT	=>	HEIGHT,
		OFF		=>	OFF,
		LOAD		=>	INVALID AND READY AND NOT done,
		CLR		=>	CLR OR change,
		CLK		=>	CLK,
		
		INVALID	=>	INVALID,
		X_OUT		=>	X_OUT,
		Y_OUT		=>	Y_OUT,
		W_OUT		=>	W_OUT,
		H_OUT		=>	H_OUT,
		OFF_OUT	=>	OFF_OUT);
	
	STAGETWO	:	Gpu_Stage_Two port map(
		X_IN			=>	X_OUT,
		Y_IN			=>	Y_OUT,
		W_IN			=>	W_OUT,
		H_IN			=>	H_OUT,
		OFF_IN		=>	OFF_OUT,
		INVALID		=>	INVALID,
		CLR			=>	CLR OR change,
		CLK			=>	CLK,
		
		PIX_OFF		=>	PIX_OFF,
		START_ADD	=>	START_ADD,
		IMAGE_ADD	=>	IMAGE_ADD,
		IMAGE_OFF	=>	IMAGE_OFF);
	
	STAGETHREE	:	Gpu_Stage_Three port map(
		START_ADD	=>	START_ADD,
		IMAGE_ADD	=>	IMAGE_ADD,
		IMAGE_OFF	=>	IMAGE_OFF,
		PIX_OFF		=>	PIX_OFF,
		CLR			=>	CLR OR change,
		CLK			=>	CLK,
		
		W_EN			=>	W_EN,
		RAM_ADDRESS	=>	RAM_ADDRESS,
		PIX_COLOR	=>	PIX_COLOR);
	
	STAGEFOUR	:	Gpu_Stage_Four port map(
		RAM_ADDRESS	=>	RAM_ADDRESS,
		PIX_COLOR	=>	PIX_COLOR,
		W_EN			=>	W_EN,
		BUFF_STAGE	=>	buff_stage,
		CLR			=>	CLR,
		CLK			=>	CLK,
		
		V_SYNC		=>	v_sync_temp,
		H_SYNC		=>	H_SYNC,
		RGB_OUT		=>	RGB_OUT);
	
	V_SYNC	<=	v_sync_temp;
end dataflow;