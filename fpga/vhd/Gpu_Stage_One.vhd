----------------------------------------------------------------------------
-- Entity:        Gpu_Stage_One
-- Written By:    Daniel Hankewycz & Robby Brague
-- Date Created:  12/04/2014
-- Description:   First stage for gpu to track image location
--
-- Revision History (date, initials, description):
-- 
-- Dependencies:
--		Reg
--		Image_Pos
----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library HDL_Cmn;
use HDL_Cmn.Components.ALL;

entity Gpu_Stage_One is
    Port ( 	X_POS		:	in		STD_LOGIC_VECTOR (15 downto 0);
				Y_POS		:	in		STD_LOGIC_VECTOR (15 downto 0);
				WIDTH		:	in		STD_LOGIC_VECTOR (7 downto 0);
				HEIGHT	:	in		STD_LOGIC_VECTOR (7 downto 0);
				OFF		:	in		STD_LOGIC_VECTOR (15 downto 0);
				LOAD		:	in		STD_LOGIC;
				CLR		:	in		STD_LOGIC;
				CLK		:	in		STD_LOGIC;
				
				INVALID	:	out	STD_LOGIC;
				X_OUT		:	out	STD_LOGIC_VECTOR (15 downto 0);
				Y_OUT		:	out	STD_LOGIC_VECTOR (15 downto 0);
				W_OUT		:	out	STD_LOGIC_VECTOR (7 downto 0);
				H_OUT		:	out	STD_LOGIC_VECTOR (7 downto 0);
				OFF_OUT	:	out	STD_LOGIC_VECTOR (15 downto 0));
end Gpu_Stage_One;

architecture dataflow of Gpu_Stage_One is
component Image_Pos is
    Port ( 	WIDTH		:	in		STD_LOGIC_VECTOR (7 downto 0);
				HEIGHT	:	in		STD_LOGIC_VECTOR (7 downto 0);
				CLR		:	in		STD_LOGIC;
				CLK		:	in		STD_LOGIC;
				
				Q			:	out	STD_LOGIC;
				W_OUT		:	out	STD_LOGIC_VECTOR (7 downto 0);
				H_OUT		:	out	STD_LOGIC_VECTOR (7 downto 0));
end component;

signal	done, invalid_tmp: std_logic;
signal	width_tmp	:	STD_LOGIC_VECTOR (7 downto 0);
signal	height_tmp	:	STD_LOGIC_VECTOR (7 downto 0);

begin
	WREG	:	Reg generic map(n=>8) port map(
		D		=>	WIDTH,
		LOAD	=>	LOAD,
		CLR	=>	CLR,
		CLK	=>	CLK,
		
		Q		=>	width_tmp);
	
	HREG	:	Reg generic map(n=>8) port map(
		D		=>	HEIGHT,
		LOAD	=>	LOAD,
		CLR	=>	CLR,
		CLK	=>	CLK,
		
		Q		=>	height_tmp);
	
	XREG	:	Reg generic map(n=>16) port map(
		D		=>	X_POS,
		LOAD	=>	LOAD,
		CLR	=>	CLR,
		CLK	=>	CLK,
		
		Q		=>	X_OUT);
	
	YREG	:	Reg generic map(n=>16) port map(
		D		=>	Y_POS,
		LOAD	=>	LOAD,
		CLR	=>	CLR,
		CLK	=>	CLK,
		
		Q		=>	Y_OUT);
		
	OFFREG	:	Reg generic map(n=>16) port map(
		D		=>	OFF,
		LOAD	=>	LOAD,
		CLR	=>	CLR,
		CLK	=>	CLK,
		
		Q		=>	OFF_OUT);
	
	IMGCOORD	:	Image_Pos port map(
		WIDTH		=> width_tmp,
		HEIGHT	=> height_tmp,
		CLR		=> CLR OR LOAD,
		CLK		=> CLK,
		
		Q			=> done,
		W_OUT		=> W_OUT,
		H_OUT		=> H_OUT);
	
	DONEREG	:	Reg generic map(n=>1) port map(
		D(0)	=>	done,
		LOAD	=>	'1',
		CLR	=>	CLR OR LOAD,
		CLK	=>	CLK,
		
		Q(0)	=>	invalid_tmp);
	
	INVALID	<=	'1' when ((invalid_tmp = '1') OR (width_tmp = x"00") OR (height_tmp = x"00")) else '0';
end dataflow;