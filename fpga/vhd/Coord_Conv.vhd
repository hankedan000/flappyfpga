----------------------------------------------------------------------------
-- Entity:        Pulse_Gen_Cnt_Out
-- Written By:    Daniel Hankewycz & Robby Brague
-- Date Created:  9/18/2014
-- Description:   Creates an nbit Pulse_Gen_Cnt_Out
--
-- Revision History (date, initials, description):
-- 
-- Dependencies:
--		Counter
--		CompareEQU
----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library HDL_Cmn;
use HDL_Cmn.Components.ALL;

entity Coord_Conv is
    Port ( 	X		:	in		STD_LOGIC_VECTOR (9 downto 0);
				Y		:	in		STD_LOGIC_VECTOR (9 downto 0);
				
				W_OUT	:	out	STD_LOGIC_VECTOR (19 downto 0));
end Coord_Conv;

architecture dataflow of Coord_Conv is


begin
	W_OUT	<= std_logic_vector(signed(X)+(320*signed(Y)));
end dataflow;