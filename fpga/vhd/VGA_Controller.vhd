----------------------------------------------------------------------------
-- Entity:        VGA_Controller
-- Written By:    Daniel Hankewycz & Robby Brague
-- Date Created:  10/15/2014
-- Description:   Generates VGA signals
--
-- Revision History (date, initials, description):
-- 
-- Dependencies:
--		
----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library HDL_Cmn;
use HDL_Cmn.Components.ALL;

entity VGA_Controller is
    Port (	RGB_IN		:	in		STD_LOGIC_VECTOR	(11  downto 0);
				CLR			:	in		STD_LOGIC;
				CLK			:	in		STD_LOGIC;
			
				H_SYNC		:	out	STD_LOGIC;
				V_SYNC		:	out	STD_LOGIC;
				X_OUT			:	out	STD_LOGIC_VECTOR	(9 downto 0);
				Y_OUT			:	out	STD_LOGIC_VECTOR	(9 downto 0);
				RGB_OUT		:	out	STD_LOGIC_VECTOR	(11  downto 0));
end VGA_Controller;

architecture structural of VGA_Controller is
-- Component Declarations

-- Intermediate signals
signal hcnt, vcnt	: STD_LOGIC_VECTOR (9 downto 0);
signal int_rgb_in	: STD_LOGIC_VECTOR (11 downto 0);
signal pxclk, hroll, hactive, hlt656, hgt751	: STD_LOGIC;
signal vroll, vactive, vlt490, vgt491	: STD_LOGIC;
	
begin
	PX_CLK_GEN: Pulse_Gen generic map(n=>2) port map(
		CNT	=> "11",
		CLR	=> CLR,
		EN		=> '1',
		CLK	=> CLK,
		
		Q		=> pxclk
	);
	
	HCOUNTER: Counter generic map(n=>10) port map(
		CLR	=> (pxclk and hroll) or CLR,
		EN		=> pxclk,
		CLK	=> CLK,
		
		Q		=> hcnt
	);
	
	VCOUNTER: Counter generic map(n=>10) port map(
		CLR	=> (pxclk and hroll and vroll) or CLR,
		EN		=> pxclk and hroll,
		CLK	=> CLK,
		
		Q		=> vcnt
	);
	
	hroll		<=	'1'	when (unsigned(hcnt) = 799) else
					'0';
	hactive	<=	'1'	when (unsigned(hcnt) < 640) else
					'0';
	hlt656	<=	'1'	when (unsigned(hcnt) < 656) else
					'0';
	hgt751	<=	'1'	when (unsigned(hcnt) > 751) else
					'0';
	
	vroll		<=	'1'	when (unsigned(vcnt) = 524) else
					'0';
	vactive	<=	'1'	when (unsigned(vcnt) < 480) else
					'0';
	vlt490	<=	'1'	when (unsigned(vcnt) < 490) else
					'0';
	vgt491	<=	'1'	when (unsigned(vcnt) > 491) else
					'0';
	
	HREG :	DFF port map(
		D		=>	hlt656 OR hgt751,
		EN		=>	'1',
		CLK	=>	CLK,
		
		Q		=> H_SYNC
	);
	
	VREG :	DFF port map(
		D		=>	vlt490 OR vgt491,
		EN		=>	'1',
		CLK	=>	CLK,
		
		Q		=> V_SYNC
	);
	
	int_rgb_in <= 	RGB_IN when ((hactive AND vactive) = '1') else
						x"000";
	
	CREG :	Reg generic map(n=>12) port map(
		D		=>	int_rgb_in,
		LOAD	=>	pxclk,
		CLK	=>	CLK,
		CLR	=> CLR,
		
		Q		=> RGB_OUT
	);
	
	X_OUT <= hcnt;
	Y_OUT	<= vcnt;
end structural;