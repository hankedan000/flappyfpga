----------------------------------------------------------------------------
-- Entity:        FlappyFPGA_top
-- Written By:    Daniel Hankewycz & Robby Brague
-- Date Created:  10/26/2014
-- Description:   Generates square and boarder graphics for the VGA driver
--
-- Revision History (date, initials, description):
-- 
-- Dependencies:
--		
----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library HDL_Cmn;
use HDL_Cmn.Components.ALL;

entity FlappyFPGA_top is
    Port (	BUTTON			:	in		STD_LOGIC_VECTOR(1 downto 0);
				SWITCH			:	in		STD_LOGIC;
				CLR				:	in		STD_LOGIC;
				CLK				:	in		STD_LOGIC;
				
				H_SYNC			:	out	STD_LOGIC;
				V_SYNC			:	out	STD_LOGIC;
				RGB				:	out	STD_LOGIC_VECTOR(11 downto 0);
				LED				: 	out 	STD_LOGIC_VECTOR(7 downto 0));	
end FlappyFPGA_top;

architecture structural of FlappyFPGA_top is
-- Component Declarations
component Z80_Computer is
    Port (	GPU_RAM_ADDR	:	in		STD_LOGIC_VECTOR(10 downto 0);
				SCREEN_DONE		:	in		STD_LOGIC_VECTOR(7 downto 0);
				USER_BUTTON		:	in		STD_LOGIC;
				RES				:	in		STD_LOGIC;
				CLK				:	in		STD_LOGIC;
	
				GPU_RAM_DATA	:	out	STD_LOGIC_VECTOR(7 downto 0);
				BUFFER_STAT		:	out	STD_LOGIC_VECTOR(7 downto 0);
				LD_STAT			:	out	STD_LOGIC;
				LED				:	out	STD_LOGIC);	
end component;

component Gpu_Top is
	Port (	SPRITE_DATA	: in STD_LOGIC_VECTOR(7 downto 0);
				DB_STAT		: in STD_LOGIC_VECTOR(7 downto 0);
				LD_STAT		: in STD_LOGIC;
				DISABLE		: in STD_LOGIC;
				BUTTON		: in STD_LOGIC;
				CLK			: in STD_LOGIC;
				CLR			: in STD_LOGIC;
				
				SPRITE_ADDR	: out STD_LOGIC_VECTOR(10 downto 0);
				V_SYNC		: out STD_LOGIC;
				H_SYNC		: out	STD_LOGIC;
				RGB_OUT		: out STD_LOGIC_VECTOR(11 downto 0);
				SCREEN_DONE	: out STD_LOGIC_VECTOR(7 downto 0));
end component;

-- Intermediate signals
signal current_cpu_addr	:	STD_LOGIC_VECTOR(15 downto 0);
signal sprite_addr_int	:	STD_LOGIC_VECTOR(10 downto 0);
signal sprite_data_int, db_stat_int, screen_done_int : STD_LOGIC_VECTOR(7 downto 0);
signal ld_stat_int, int_led	: STD_LOGIC;

-- Alias
	
begin
	COMPUTER: Z80_Computer port map(
		GPU_RAM_ADDR	=> sprite_addr_int,
		SCREEN_DONE		=> screen_done_int,
		USER_BUTTON		=>	BUTTON(1),
		RES				=> CLR,
		CLK				=> CLK,
		
		GPU_RAM_DATA	=> sprite_data_int,
		BUFFER_STAT		=> db_stat_int,
		LD_STAT			=> ld_stat_int,
		LED				=> int_led);
		
	GPU: Gpu_Top port map(
		SPRITE_DATA => sprite_data_int,
		DB_STAT		=> db_stat_int,
		LD_STAT		=> ld_stat_int,
		DISABLE		=>	SWITCH,
		BUTTON		=>	BUTTON(0),
		CLK			=> CLK,
		CLR			=> CLR,
		
		SPRITE_ADDR	=> sprite_addr_int,
		V_SYNC		=> V_SYNC,
		H_SYNC		=> H_SYNC,
		RGB_OUT		=> RGB,
		SCREEN_DONE	=> screen_done_int);
		
		LED <= int_led & screen_done_int(6 downto 0);
end structural;