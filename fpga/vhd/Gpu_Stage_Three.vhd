----------------------------------------------------------------------------
-- Entity:        Gpu_Stage_Three
-- Written By:    Daniel Hankewycz & Robby Brague
-- Date Created:  12/04/2014
-- Description:   First stage for gpu to track image location
--
-- Revision History (date, initials, description):
-- 
-- Dependencies:
--		Reg
--		Image_Pos
----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library HDL_Cmn;
use HDL_Cmn.Components.ALL;

entity Gpu_Stage_Three is
    Port ( 	START_ADD	:	in		STD_LOGIC_VECTOR (19 downto 0);
				IMAGE_ADD	:	in		STD_LOGIC_VECTOR (19 downto 0);
				IMAGE_OFF	:	in		STD_LOGIC_VECTOR (15 downto 0);
				PIX_OFF		:	in		STD_LOGIC;
				CLR			:	in		STD_LOGIC;
				CLK			:	in		STD_LOGIC;
				
				W_EN			:	out	STD_LOGIC;
				RAM_ADDRESS	:	out	STD_LOGIC_VECTOR (19 downto 0);
				PIX_COLOR	:	out	STD_LOGIC_VECTOR (7 downto 0));
end Gpu_Stage_Three;
architecture dataflow of Gpu_Stage_Three is
component Sheet is
	port(
		CE_n	: in std_logic;
		OE_n	: in std_logic;
		A		: in std_logic_vector(15 downto 0);
		
		D		: out std_logic_vector(7 downto 0));
end component;

signal	tmp_ram_address : std_logic_vector(19 downto 0);
signal	sheet_addr	: STD_LOGIC_VECTOR(19 downto 0);
signal	tmp_color : std_logic_vector(7 downto 0);

begin
	tmp_ram_address	<=	std_logic_vector(signed(START_ADD) + signed(IMAGE_ADD));
	
	RAMREG	:	Reg generic map(n=>20) port map(
		D		=>	tmp_ram_address,
		LOAD	=>	'1',
		CLR	=>	CLR,
		CLK	=>	CLK,
		
		Q		=>	RAM_ADDRESS);
		
	WREG	:	Reg generic map(n=>1) port map(
		D(0)	=>	NOT PIX_OFF,
		LOAD	=>	'1',
		CLR	=>	CLR,
		CLK	=>	CLK,
		
		Q(0)	=>	W_EN);
	
	sheet_addr	<=	std_logic_vector(signed(x"0"&IMAGE_OFF) + signed(IMAGE_ADD));
	
	SPRITE_SHEET: Sheet port map(
		A		=> sheet_addr(15 downto 0),
		D		=> tmp_color,
		CE_n	=> '0',
		OE_n	=> '0');
	
	COLORREG	:	Reg generic map(n=>8) port map(
		D		=>	tmp_color,
		LOAD	=>	'1',
		CLR	=>	CLR,
		CLK	=>	CLK,
		
		Q		=>	PIX_COLOR);
end dataflow;