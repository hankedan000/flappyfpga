----------------------------------------------------------------------------
-- Entity:        Button_Catch
-- Written By:    Daniel Hankewycz & Robby Brague
-- Date Created:  12/07/2014
-- Description:   Captures a button input
--
-- Revision History (date, initials, description):
-- 
-- Dependencies:
--		
--		
----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library HDL_Cmn;
use HDL_Cmn.Components.ALL;

entity Button_Catch is
    Port ( 	BUTTON	:	in		STD_LOGIC_VECTOR(7 downto 0);
				CLR		:	in		STD_LOGIC;
				CLK		:	in		STD_LOGIC;
				
				Q			:	out	STD_LOGIC_VECTOR(7 downto 0));
end Button_Catch;

architecture dataflow of Button_Catch is

signal	tmp_bounce, pressed	:	STD_LOGIC_VECTOR(7 downto 0);

begin
	BOUNCER	:	Debouncer generic map(n=>8, delay_width=>4) port map(
		D		=>	BUTTON,
		CNT	=>	x"F",
		CLK	=>	CLK,
		
		Q		=>	tmp_bounce);
	
	SHOT_0	:	OneShot port map(
		X		=>	tmp_bounce(0),
		CLK	=>	CLK,
		
		Y		=>	pressed(0));
	
	REG_0	:	Reg generic map(n=>1) port map(
		D(0)	=>	'1',
		LOAD	=>	pressed(0),
		CLR	=>	CLR,
		CLK	=>	CLK,
		
		Q(0)	=>	Q(0));
	
	SHOT_1	:	OneShot port map(
		X		=>	tmp_bounce(1),
		CLK	=>	CLK,
		
		Y		=>	pressed(1));
	
	REG_1	:	Reg generic map(n=>1) port map(
		D(0)	=>	'1',
		LOAD	=>	pressed(1),
		CLR	=>	CLR,
		CLK	=>	CLK,
		
		Q(0)	=>	Q(1));
	
	SHOT_2	:	OneShot port map(
		X		=>	tmp_bounce(2),
		CLK	=>	CLK,
		
		Y		=>	pressed(2));
	
	REG_2	:	Reg generic map(n=>1) port map(
		D(0)	=>	'1',
		LOAD	=>	pressed(2),
		CLR	=>	CLR,
		CLK	=>	CLK,
		
		Q(0)	=>	Q(2));
	
	SHOT_3	:	OneShot port map(
		X		=>	tmp_bounce(3),
		CLK	=>	CLK,
		
		Y		=>	pressed(3));
	
	REG_3	:	Reg generic map(n=>1) port map(
		D(0)	=>	'1',
		LOAD	=>	pressed(3),
		CLR	=>	CLR,
		CLK	=>	CLK,
		
		Q(0)	=>	Q(3));
	
	SHOT_4	:	OneShot port map(
		X		=>	tmp_bounce(4),
		CLK	=>	CLK,
		
		Y		=>	pressed(4));
	
	REG_4	:	Reg generic map(n=>1) port map(
		D(0)	=>	'1',
		LOAD	=>	pressed(4),
		CLR	=>	CLR,
		CLK	=>	CLK,
		
		Q(0)	=>	Q(4));
	
	SHOT_5	:	OneShot port map(
		X		=>	tmp_bounce(5),
		CLK	=>	CLK,
		
		Y		=>	pressed(5));
	
	REG_5	:	Reg generic map(n=>1) port map(
		D(0)	=>	'1',
		LOAD	=>	pressed(5),
		CLR	=>	CLR,
		CLK	=>	CLK,
		
		Q(0)	=>	Q(5));
	
	SHOT_6	:	OneShot port map(
		X		=>	tmp_bounce(6),
		CLK	=>	CLK,
		
		Y		=>	pressed(6));
	
	REG_6	:	Reg generic map(n=>1) port map(
		D(0)	=>	'1',
		LOAD	=>	pressed(6),
		CLR	=>	CLR,
		CLK	=>	CLK,
		
		Q(0)	=>	Q(6));
	
	SHOT_7	:	OneShot port map(
		X		=>	tmp_bounce(7),
		CLK	=>	CLK,
		
		Y		=>	pressed(7));
	
	REG_7	:	Reg generic map(n=>1) port map(
		D(0)	=>	'1',
		LOAD	=>	pressed(7),
		CLR	=>	CLR,
		CLK	=>	CLK,
		
		Q(0)	=>	Q(7));
end dataflow;