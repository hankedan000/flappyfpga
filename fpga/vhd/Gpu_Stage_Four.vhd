----------------------------------------------------------------------------
-- Entity:        Gpu_Stage_Four
-- Written By:    Daniel Hankewycz & Robby Brague
-- Date Created:  12/04/2014
-- Description:   First stage for gpu to track image location
--
-- Revision History (date, initials, description):
-- 
-- Dependencies:
--		Reg
--		Image_Pos
----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library HDL_Cmn;
use HDL_Cmn.Components.ALL;

entity Gpu_Stage_Four is
    Port ( 	RAM_ADDRESS	:	in		STD_LOGIC_VECTOR (19 downto 0);
				PIX_COLOR	:	in		STD_LOGIC_VECTOR (7 downto 0);
				BUFF_STAGE	:	in		STD_LOGIC;
				W_EN			:	in		STD_LOGIC;
				CLR			:	in		STD_LOGIC;
				CLK			:	in		STD_LOGIC;
				
				V_SYNC		: 	out	STD_LOGIC;
				H_SYNC		: 	out	STD_LOGIC;
				RGB_OUT		: 	out	STD_LOGIC_VECTOR(11 downto 0));
end Gpu_Stage_Four;
architecture dataflow of Gpu_Stage_Four is
component true_dpram_sclk is
generic (n:integer:=8);
	port (	
		data_a	: in std_logic_vector(7 downto 0);
		data_b	: in std_logic_vector(7 downto 0);
		addr_a	: in natural range 0 to (2**n)-1;
		addr_b	: in natural range 0 to (2**n)-1;
		we_a		: in std_logic := '1';
		we_b		: in std_logic := '1';
		clk		: in std_logic;
		q_a		: out std_logic_vector(7 downto 0);
		q_b		: out std_logic_vector(7 downto 0)
	);
end component;

component VGA_Controller is
    Port (	RGB_IN		:	in		STD_LOGIC_VECTOR	(11  downto 0);
				CLR			:	in		STD_LOGIC;
				CLK			:	in		STD_LOGIC;
			
				H_SYNC		:	out	STD_LOGIC;
				V_SYNC		:	out	STD_LOGIC;
				X_OUT			:	out	STD_LOGIC_VECTOR	(9 downto 0);
				Y_OUT			:	out	STD_LOGIC_VECTOR	(9 downto 0);
				RGB_OUT		:	out	STD_LOGIC_VECTOR	(11  downto 0));
end component;

signal 	xcnt, ycnt		: STD_LOGIC_VECTOR (9 downto 0);
signal	rgb_mem	:	STD_LOGIC_VECTOR(7 downto 0);
signal	red,green,blue	: STD_LOGIC_VECTOR(3 downto 0);
signal	tmp_w_en, color_check	:	std_logic;
signal 	rgb_addr			:	integer range 0 to (2**18)-1;
signal 	rgb_addr_shift	:	integer range 0 to (2**18)-1;
signal 	ram_in			:	integer range 0 to (2**18)-1;
signal 	ram_in_shift	:	integer range 0 to (2**18)-1;

begin
	color_check	<=	'1'	when	("0" & PIX_COLOR > "0" & x"7F") else '0';
	tmp_w_en		<=	W_EN AND color_check;
	
	red		<= rgb_mem(5)&rgb_mem(4)&rgb_mem(5)&rgb_mem(4);
	green		<= rgb_mem(3)&rgb_mem(2)&rgb_mem(3)&rgb_mem(2);
	blue		<= rgb_mem(1)&rgb_mem(0)&rgb_mem(1)&rgb_mem(0);
	
	VGA: VGA_Controller port map(
		RGB_IN 	=> red&green&blue,
		CLR		=> '0',
		CLK 		=> CLK,
		H_SYNC 	=> H_SYNC,
		V_SYNC 	=> V_SYNC,
		X_OUT 	=> xcnt,
		Y_OUT 	=> ycnt,
		RGB_OUT 	=> RGB_OUT);
	
	rgb_addr				<= to_integer(unsigned(xcnt(9 downto 1)))+(to_integer(unsigned(ycnt(9 downto 1)))*320);
	rgb_addr_shift		<=	(rgb_addr + 131072) when (BUFF_STAGE = '1') else rgb_addr;
	
	ram_in			<=	to_integer(unsigned(RAM_ADDRESS));
	ram_in_shift	<=	(ram_in + 131072) when (BUFF_STAGE = '0') else ram_in;
	
	VGA_RAM: true_dpram_sclk generic map(n=>18) port map(
		data_a	=> PIX_COLOR,
		data_b	=> x"00",
		addr_a	=> ram_in_shift,
		addr_b	=> rgb_addr_shift,
		we_a		=> tmp_w_en,
		we_b		=> '0',
		clk		=> CLK,
		q_a		=> open,
		q_b		=> rgb_mem);
end dataflow;