----------------------------------------------------------------------------
-- Entity:        Z80_Computer
-- Written By:    Daniel Hankewycz & Robby Brague
-- Date Created:  10/27/2014
-- Description:   Z80 CPU with RAM and ROM built in
--
-- Revision History (date, initials, description):
-- 
-- Dependencies:
--		
----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library HDL_Cmn;
use HDL_Cmn.Components.ALL;

entity Z80_Computer is
    Port (	GPU_RAM_ADDR	:	in		STD_LOGIC_VECTOR(10 downto 0);
				SCREEN_DONE		:	in		STD_LOGIC_VECTOR(7 downto 0);
				USER_BUTTON		:	in		STD_LOGIC;
				RES				:	in		STD_LOGIC;
				CLK				:	in		STD_LOGIC;
				
				GPU_RAM_DATA	:	out	STD_LOGIC_VECTOR(7 downto 0);
				BUFFER_STAT		:	out	STD_LOGIC_VECTOR(7 downto 0);
				LD_STAT			:	out	STD_LOGIC;
				LED				:	out	STD_LOGIC);	
end Z80_Computer;

architecture structural of Z80_Computer is
-- Component Declarations
component T80s is
	generic(
		Mode : integer := 0;	-- 0 => Z80, 1 => Fast Z80, 2 => 8080, 3 => GB
		T2Write : integer := 0;	-- 0 => WR_n active in T3, /=0 => WR_n active in T2
		IOWait : integer := 1	-- 0 => Single cycle I/O, 1 => Std I/O cycle
	);
	port(
		RESET_n	: in 	std_logic;
		CLK_n		: in 	std_logic;
		WAIT_n	: in 	std_logic;
		INT_n		: in 	std_logic;
		NMI_n		: in 	std_logic;
		BUSRQ_n	: in 	std_logic;
		M1_n		: out std_logic;
		MREQ_n	: out std_logic;
		IORQ_n	: out std_logic;
		RD_n		: out std_logic;
		WR_n		: out std_logic;
		RFSH_n	: out std_logic;
		HALT_n	: out std_logic;
		BUSAK_n	: out std_logic;
		A			: out std_logic_vector(15 downto 0);
		DI			: in 	std_logic_vector(7 downto 0);
		DO			: out std_logic_vector(7 downto 0)
	);
end component;

component true_dpram_sclk is
generic (n:integer:=8);
	port (	
		data_a	: in std_logic_vector(7 downto 0);
		data_b	: in std_logic_vector(7 downto 0);
		addr_a	: in natural range 0 to (2**n)-1;
		addr_b	: in natural range 0 to (2**n)-1;
		we_a		: in std_logic := '1';
		we_b		: in std_logic := '1';
		clk		: in std_logic;
		q_a		: out std_logic_vector(7 downto 0);
		q_b		: out std_logic_vector(7 downto 0)
	);
end component;

component ROM is
	port(
		CE_n	: in 		std_logic;
		OE_n	: in 		std_logic;
		A		: in 		std_logic_vector(14 downto 0);
		D		: out 	std_logic_vector(7 downto 0)
	);
end component;

component Button_Catch is
    Port ( 	BUTTON	:	in		STD_LOGIC_VECTOR(7 downto 0);
				CLR		:	in		STD_LOGIC;
				CLK		:	in		STD_LOGIC;
				
				Q			:	out	STD_LOGIC_VECTOR(7 downto 0));
end component;

-- Intermediate signals
signal	int_addr, good_addr, sys_timer, gpu_addr_tmp						:	STD_LOGIC_VECTOR(15 downto 0);
signal	cpu_data_in, cpu_data_out, rom_data, ram_data,rand_counter	:	STD_LOGIC_VECTOR(7 downto 0);
signal	clk_16MHZ, pulse_16MHZ, cpu_w, cpu_r, mreq_n, ram_ce, rom_ce, ld_stat_int, ld_stat_shot	:	STD_LOGIC;
signal	buff_stage, change						:	STD_LOGIC;
signal	buffer_stat_temp, button_pressed		:	STD_LOGIC_VECTOR(7 downto 0);

-- Alias
alias	timer_LO	: STD_LOGIC_VECTOR is sys_timer(7 downto 0);
alias	timer_HI	: STD_LOGIC_VECTOR is sys_timer(15 downto 8);
	
begin		
	PULSE_4:	Pulse_Gen generic map(n=>3) port map(
		CNT	=> "010",
		CLR	=> RES,
		EN		=> '1',
		CLK	=> CLK,
		
		Q		=> pulse_16MHZ);
		
	CLK_4: TFF port map(
		TOG	=> pulse_16MHZ,
		CLR	=> RES,
		CLK	=> CLK,
		
		Q		=> clk_16MHZ);
		
	TIMER: Clock_Divided_Counter generic map(n=>16,timer_width=>17) port map(
		CNT	=> "11000011010100000",	-- counts by milliseconds
		CLR	=> RES,
		EN		=> '1',
		CLK	=> CLK,
		
		Q		=> sys_timer);
	
	RAND	:	Counter generic map(n=>8) port map(
		CLR	=>	'0',
		EN		=>	'1',
		CLK	=>	CLK,
		
		Q		=>	rand_counter);
	
	BUFFREG	:	Reg generic map(n=>1) port map(
		D(0)	=>	buffer_stat_temp(0),
		LOAD	=>	ld_stat_shot,
		CLR	=>	RES,
		CLK	=>	CLK,
		
		Q(0)	=>	buff_stage);
	
	change	<=	'1' when (buff_stage /= buffer_stat_temp(0)) AND (ld_stat_shot = '1') else '0';
	
	INPUT	:	Button_Catch port map(
		BUTTON	=>	"0000000" & USER_BUTTON,
		CLR		=> RES OR change,
		CLK		=>	CLK,
		
		Q			=>	button_pressed);
	
	LED	<=	button_pressed(0);
	
	CPU_ROM: ROM port map(
		CE_n	=> rom_ce,
		OE_n	=> cpu_r and not cpu_w,
		A		=> good_addr(14 downto 0),
		D		=> rom_data);
	
	gpu_addr_tmp	<= std_logic_vector(unsigned("00000"&GPU_RAM_ADDR)+26816);
	
	CPU_RAM: true_dpram_sclk generic map(n=>15) port map(
		data_a	=> cpu_data_out,
		data_b	=> x"00",
		addr_a	=> to_integer(unsigned(int_addr(14 downto 0))),
		addr_b	=> to_integer(unsigned(gpu_addr_tmp)),			-- SPRITE_MEM offset
		we_a		=> not cpu_w,
		we_b		=> '0',
		clk		=> CLK,
		q_a		=> ram_data,
		q_b		=> GPU_RAM_DATA);

	CPU: T80s port map(
		RESET_n	=> not RES,
		CLK_n		=> clk_16MHZ,
		WAIT_n	=> '1',
		INT_n		=> '1',
		NMI_n		=> '1',
		M1_n		=> open,
		IORQ_n	=> open,
		BUSRQ_n	=> '1',
		MREQ_n	=> mreq_n,
		RD_n		=> cpu_r,
		WR_n		=> cpu_w,
		A			=> int_addr,
		DI			=> cpu_data_in,
		DO			=> cpu_data_out);
		
	-- ADDRESS REGISTER
	process(CLK, mreq_n)
	begin
		if(CLK'event and CLK='1') then
			if(mreq_n='0') then
				good_addr <= int_addr;
			end if;
		end if;
	end process;
		
	rom_ce	<= '0' when good_addr(15)='0' else
					'1';
					
	ram_ce	<= '0' when good_addr(15)='1' else
					'1';
	
	cpu_data_in	<= SCREEN_DONE		when good_addr=x"DF05" else
						timer_LO			when good_addr=x"DF02" else
						timer_HI 		when good_addr=x"DF03" else
						rand_counter	when good_addr=x"DF06" else
						button_pressed	when good_addr=x"DF00" else
						rom_data 		when rom_ce='0' else
						ram_data 		when ram_ce='0' else
						rom_data;
				
	buffer_stat_temp	<= cpu_data_out when (ld_stat_shot='1') else
								x"00";
	BUFFER_STAT	<=	buffer_stat_temp;
	
	LD_SHOT : OneShot port map(
		X		=> ld_stat_int,
		CLK	=> CLK,
		
		Y		=> ld_stat_shot);
	ld_stat_int		<= '1' when (good_addr=x"DF04" and cpu_w='0') else
							'0';
	LD_STAT	<= ld_stat_shot;
end structural;