----------------------------------------------------------------------------
-- Entity:        Gpu_Stage_Two
-- Written By:    Daniel Hankewycz & Robby Brague
-- Date Created:  12/04/2014
-- Description:   First stage for gpu to track image location
--
-- Revision History (date, initials, description):
-- 
-- Dependencies:
--		Reg
--		Image_Pos
----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library HDL_Cmn;
use HDL_Cmn.Components.ALL;

entity Gpu_Stage_Two is
    Port ( 	X_IN			:	in		STD_LOGIC_VECTOR (15 downto 0);
				Y_IN			:	in		STD_LOGIC_VECTOR (15 downto 0);
				W_IN			:	in		STD_LOGIC_VECTOR (7 downto 0);
				H_IN			:	in		STD_LOGIC_VECTOR (7 downto 0);
				OFF_IN		:	in		STD_LOGIC_VECTOR (15 downto 0);
				INVALID		:	in		STD_LOGIC;
				CLR			:	in		STD_LOGIC;
				CLK			:	in		STD_LOGIC;
				
				PIX_OFF		:	out	STD_LOGIC;
				START_ADD	:	out	STD_LOGIC_VECTOR (19 downto 0);
				IMAGE_ADD	:	out	STD_LOGIC_VECTOR (19 downto 0);
				IMAGE_OFF	:	out	STD_LOGIC_VECTOR (15 downto 0));
end Gpu_Stage_Two;
architecture dataflow of Gpu_Stage_Two is
component Coord_Conv is
	 Port ( 	X		:	in		STD_LOGIC_VECTOR (9 downto 0);
				Y		:	in		STD_LOGIC_VECTOR (9 downto 0);
				
				W_OUT	:	out	STD_LOGIC_VECTOR (19 downto 0));
end component;

component Bound_Check is
generic (n:integer:=4);
	 Port ( 	A			:	in		STD_LOGIC_VECTOR (n-1 downto 0);
				X			:	in		STD_LOGIC_VECTOR (n-1 downto 0);
				B			:	in		STD_LOGIC_VECTOR (n-1 downto 0);
				
				IN_RANGE	:	out	STD_LOGIC);
end component;

signal	tmp_start_coord, tmp_image_coord : std_logic_vector(19 downto 0);
signal	x_add, y_add : std_logic_vector(15 downto 0);
signal	x_check, y_check : std_logic;

begin
	STARTCOORD	:	Coord_Conv port map(
		X		=> X_IN(9 downto 0),
		Y		=> Y_IN(9 downto 0),
		
		W_OUT	=> tmp_start_coord);
	
	STARTREG	:	Reg generic map(n=>20) port map(
		D		=>	tmp_start_coord,
		LOAD	=>	'1',
		CLR	=>	CLR,
		CLK	=>	CLK,
		
		Q		=>	START_ADD);
	
	IMGCOORD	:	Coord_Conv port map(
		X		=> "00" & W_IN,
		Y		=> "00" & H_IN,
		
		W_OUT	=> tmp_image_coord);
	
	IMGREG	:	Reg generic map(n=>20) port map(
		D		=>	tmp_image_coord,
		LOAD	=>	'1',
		CLR	=>	CLR,
		CLK	=>	CLK,
		
		Q		=>	IMAGE_ADD);
	
	x_add		<=	std_logic_vector(signed(X_IN) + signed(x"00" & W_IN));
	y_add		<=	std_logic_vector(signed(Y_IN) + signed(x"00" & H_IN));
	
	XCHECK	:	Bound_Check generic map(n=>16) port map(
		A			=> x"FFFF",
		X			=> x_add,
		B			=> x"0140",
		
		IN_RANGE => x_check);
	
	YCHECK	:	Bound_Check generic map(n=>16) port map(
		A			=> x"FFFF",
		X			=> y_add,
		B			=> x"00F0",
		
		IN_RANGE => y_check);
	
	PIXREG	:	Reg generic map(n=>1) port map(
		D(0)	=>	NOT x_check OR NOT y_check OR INVALID,
		LOAD	=>	'1',
		CLR	=>	CLR,
		CLK	=>	CLK,
		
		Q(0)	=>	PIX_OFF);
	
	OFFREG	:	Reg generic map(n=>16) port map(
		D		=>	OFF_IN,
		LOAD	=>	'1',
		CLR	=>	CLR,
		CLK	=>	CLK,
		
		Q		=>	IMAGE_OFF);
end dataflow;