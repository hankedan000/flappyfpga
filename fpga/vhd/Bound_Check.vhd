----------------------------------------------------------------------------
-- Entity:        Bound_Check
-- Written By:    Daniel Hankewycz & Robby Brague
-- Date Created:  12/04/2014
-- Description:   checks that a < x < b
--
-- Revision History (date, initials, description):
-- 
-- Dependencies:
--		none
----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Bound_Check is
generic (n:integer:=4);
    Port ( 	A			:	in		STD_LOGIC_VECTOR (n-1 downto 0);
				X			:	in		STD_LOGIC_VECTOR (n-1 downto 0);
				B			:	in		STD_LOGIC_VECTOR (n-1 downto 0);
				
				IN_RANGE	:	out	STD_LOGIC);
end Bound_Check;

architecture dataflow of Bound_Check is
begin
	with ((signed(A)<signed(X)) AND  (signed(X)<signed(B))) select
		IN_RANGE	<= '1' when true,
					'0' when others;
end dataflow;