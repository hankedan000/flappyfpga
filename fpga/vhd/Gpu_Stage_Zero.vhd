----------------------------------------------------------------------------
-- Entity:        Gpu_Stage_Zero
-- Written By:    Daniel Hankewycz & Robby Brague
-- Date Created:  10/9/2014
-- Description:   FSM for decoding keyboard data
--
-- Revision History (date, initials, description):
-- 
-- Dependencies:
--		
----------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library HDL_Cmn;
use HDL_Cmn.Components.ALL;

entity Gpu_Stage_Zero is
    Port ( 	NEXT_SPRITE	: in 	STD_LOGIC;
				SPRITE_DATA	: in	STD_LOGIC_VECTOR(7 downto 0);
				BUFF_STAGE	: in	STD_LOGIC;
				CLK			: in	STD_LOGIC;
				CLR			: in 	STD_LOGIC;
				
				SPRITE_ADDR	: out STD_LOGIC_VECTOR(10 downto 0);
				X				: out STD_LOGIC_VECTOR(15 downto 0);
				Y				: out STD_LOGIC_VECTOR(15 downto 0);
				WIDTH			: out STD_LOGIC_VECTOR(7 downto 0);
				HEIGHT		: out STD_LOGIC_VECTOR(7 downto 0);
				SHEET_OFFSET: out STD_LOGIC_VECTOR(15 downto 0);
				DONE			: out STD_LOGIC;
				READY			: out STD_LOGIC);
end Gpu_Stage_Zero;

architecture structural of Gpu_Stage_Zero is
-- Component Declaration

-- Intermediate Signals
signal arg_index : STD_LOGIC_VECTOR(3 downto 0);
signal sprite_index : STD_LOGIC_VECTOR(9 downto 0);
signal x_l,x_h,y_l,y_h,w,h,sheet_l,sheet_h : STD_LOGIC_VECTOR(7 downto 0);
signal sprite_loaded, sprite_cnt_clr : STD_LOGIC;
signal sprite_addr_tmp	:	unsigned(9 downto 0);

begin
	ARG_COUNTER: Counter generic map(n=>4) port map(
		CLR		=> CLR or NEXT_SPRITE,
		EN			=> not sprite_loaded,
		CLK		=> CLK,
		
		Q			=> arg_index);
		
	sprite_loaded <= 	'1' when arg_index=x"9" else
							'0';
							
	SPRITE_COUNTER: UpDown_Counter generic map(n=>10) port map(
		CNT		=> "0000001000",
		SUBTRACT	=> '0',
		EN			=> NEXT_SPRITE,
		CLR		=> CLR or sprite_cnt_clr,
		CLK		=> CLK,
		
		Q			=> sprite_index);
		
	sprite_cnt_clr <= '1' when sprite_index="1100000000" else	-- when =768
							'0';
		
	SHEET_H_REG: Reg generic map(n=>8) port map(
		D			=> SPRITE_DATA,
		LOAD		=> not sprite_loaded,
		CLK		=> CLK,
		CLR		=> CLR,
		
		Q			=> sheet_h);
		
	SHEET_L_REG: Reg generic map(n=>8) port map(
		D			=> sheet_h,
		LOAD		=> not sprite_loaded,
		CLK		=> CLK,
		CLR		=> CLR,
		
		Q			=> sheet_l);
		
	Y_H_REG: Reg generic map(n=>8) port map(
		D			=> sheet_l,
		LOAD		=> not sprite_loaded,
		CLK		=> CLK,
		CLR		=> CLR,
		
		Q			=> y_h);
		
	Y_L_REG: Reg generic map(n=>8) port map(
		D			=> y_h,
		LOAD		=> not sprite_loaded,
		CLK		=> CLK,
		CLR		=> CLR,
		
		Q			=> y_l);
		
	X_H_REG: Reg generic map(n=>8) port map(
		D			=> y_l,
		LOAD		=> not sprite_loaded,
		CLK		=> CLK,
		CLR		=> CLR,
		
		Q			=> x_h);
		
	X_L_REG: Reg generic map(n=>8) port map(
		D			=> x_h,
		LOAD		=> not sprite_loaded,
		CLK		=> CLK,
		CLR		=> CLR,
		
		Q			=> x_l);
		
	H_REG: Reg generic map(n=>8) port map(
		D			=> x_l,
		LOAD		=> not sprite_loaded,
		CLK		=> CLK,
		CLR		=> CLR,
		
		Q			=> h);
		
	W_REG: Reg generic map(n=>8) port map(
		D			=> h,
		LOAD		=> not sprite_loaded,
		CLK		=> CLK,
		CLR		=> CLR,
		
		Q			=> w);
	
	sprite_addr_tmp 	<= unsigned(sprite_index)+unsigned(arg_index);
	SPRITE_ADDR			<=	'1' & std_logic_vector(sprite_addr_tmp ) when (BUFF_STAGE = '1') else '0' & std_logic_vector(sprite_addr_tmp);
	DONE					<= '1' when (unsigned(sprite_index) = 760) else '0';
	READY 				<= sprite_loaded;
	X						<= x_h&x_l;
	Y						<= y_h&y_l;
	WIDTH					<= w;
	HEIGHT				<= h;
	SHEET_OFFSET 		<= sheet_h&sheet_l;
end structural;
