#include <iostream>
#include <fstream>
#include <iomanip>

#define     WIDTH_ADDR          0x12
#define     HEIGHT_ADDR         0x16
#define     BPP_ADDR            0x1C
#define     IMG_BEG_LOC_ADDR    0x0A

using namespace std;

fstream fin;
bool alpha = false;
string image_name = "image";
int width;
int height;
int image_offset;
int bpp;
int image_size_bytes;

int getPixel(int x, int y);
void printAsmImage();
void printImage();

int main(int argc, char* argv[])
{
    for(int i=1; i<argc; i++){
        switch(i){
            case 1:
                fin.open(argv[1]);
                break;
            case 2:
                image_name= string(argv[2]);
                break;
            case 3:
                alpha=true;
                break;
            default:
                cout << "BAD ARGUMENT" << endl;
        }// switch
    }// for

    /* GET IMAGE PARAMETERS */
    fin.seekg(WIDTH_ADDR);
    width           = fin.get();
    width           |= (fin.get()<<8);
    width           |= (fin.get()<<16);
    width           |= (fin.get()<<24);
    fin.seekg(HEIGHT_ADDR);
    height          = fin.get();
    height          |= (fin.get()<<8);
    height          |= (fin.get()<<16);
    height          |= (fin.get()<<24);
    fin.seekg(IMG_BEG_LOC_ADDR);
    image_offset    = fin.get();
    fin.seekg(BPP_ADDR);
    bpp             = fin.get();
    image_size_bytes = (bpp/8)*width*height;

    cout << "IMG DIM:\t" << width << 'x' << height << endl;
    cout << "IMG OFFSET:\t" << image_offset << endl;
    cout << "BPP:\t\t" << bpp << endl;
    cout << "IMG (BYTES):\t" << image_size_bytes << endl;

//    cout << hex << getPixel(width-1,0) << endl;
    printAsmImage();

    return 0;
}

int getPixel(int x, int y){
    int row_padding;
    if((((bpp/8)*width)%4)!=0)  row_padding = 4-(((bpp/8)*width)%4);
    else                        row_padding = 0;
    int x_offset = (bpp/8)*(width-x-1)+(row_padding*(y+1))+(bpp/8);
    int y_offset = (bpp/8)*width*y;
    int offset = 0-x_offset-y_offset;
    int result = 0;

    fin.seekg(offset, ios_base::end);
    if(alpha){
        result |= fin.get()<<24;
        result |= fin.get();
        result |= fin.get()<<8;
        result |= fin.get()<<16;
    } else {
        result |= fin.get();
        result |= fin.get()<<8;
        result |= fin.get()<<16;
    }// if

    return result;
}// getPixel()

void printAsmImage(){
    cout << image_name << ':' << endl;
//    cout << ".db\t\t";
//    cout << '$' << setfill('0') << setw(2) << hex << width << ',';
//    cout << '$' << setfill('0') << setw(2) << hex << height << endl;
    for(int y=0; y<height; y++){
//        cout << ".db\t\t";
        for(int x=0; x<width; x++){
            int pixel = getPixel(x,y);
            int a = ((pixel&0xFF000000)>>24)/64;
            int r = ((pixel&0x00FF0000)>>16)/64;
            int g = ((pixel&0x0000FF00)>>8 )/64;
            int b = ((pixel&0x000000FF)>>0 )/64;
            cout << dec << x+(y*320) << " => \t x\""<< setfill('0') << setw(2) << hex << ((a<<6)|(r<<4)|(g<<2)|(b<<0)) << "\",\n";
        }// for
//        cout << endl;
    }// for
}// printAsmImage()

void printImage(){
    cout << endl;
    for(int y=0; y<height; y++){
        for(int x=0; x<width; x++){
            switch(getPixel(x,y)){
                case 0xff:
                    cout << 'B';
                    break;
                case 0xff00:
                    cout << 'G';
                    break;
                case 0xff0000:
                    cout << 'R';
                    break;
                default:
                    cout << 'X';
                    break;
            }// switch
        }// for
        cout << endl;
    }// for
}// printImage()
